# RustyBind - FFI bindings generator for Rust

This is a Rust crate for generating Swift, C++, SWIG and WebAssembly glue code out of the input rust module containing declarations of types and functions. SWIG gluecode code can be further use to implement FFI for C#, Java, Python etc. C++ code can be used as an input for Emscripten compiler in order to generate WebAssembly binary and JS gluecode. See the `example/` directory.


**NOTE:** This crate is in an experimental/development phase and should not be considered as ready-to-use in any stable project.

## Setup

The following instruction is a good starting point to generate binding code in your project:

#### 0. Add the following dependencies to the `Cargo.toml` file:

```toml
[lib]
crate-type = ["staticlib", "lib"]
# [ ... ]

[dependencies]
rusty-bind = { version = "0.1.0" }
# [ ... ]

[build-dependencies]
rusty-bind-build = { version = "0.1.0" }
```

#### 1. Prepare a `build.rs` file, e.g.:

```rust
fn main() {
    use rusty_bind_build::parse_ffi_module;
    parse_ffi_module("src/ffi/mod.rs", "./_generated_ffi_code/").unwrap();
    println!("cargo:rerun-if-changed=src/ffi/mod.rs");
}
```

#### 2. Prepare an FFI module and add a single attribute `#[binding_wrapper]` with a source parameter like in the following example:

```rust
//
// File: src/ffi/mod.rs
//

use rusty_bind::binding_wrapper;
use std::sync::{Arc, Mutex};

// Define Error type and `()` type.
#[derive(Clone, Debug)]
#[repr(C)]
pub enum ErrorType {
    Error
}
pub trait ExceptionTrait {
    fn reason(&self) -> String;
}
impl ExceptionTrait for ErrorType {
    fn reason(&self) -> String {
        match self {
            ErrorType::Error => {
                "Error Message".to_owned()
            }
        }
    }
}

pub trait SomeTrait: std::fmt::Debug {
    fn some_trait_method(&self);
}

#[derive(Clone, Debug)]
pub struct Foo(u32);
impl SomeTrait for Foo {
    fn some_trait_method(&self) {
    }
}

#[derive(Clone, Debug)]
pub struct CustomType(u32);
impl CustomType {
    pub fn return_result_with_dynamic_type(&self) -> Result<Arc<Mutex<dyn SomeTrait>>, ErrorType> {
        Ok(Arc::new(Mutex::new(Foo(10u32))))
    }
    pub fn return_another_custom_type(&self) -> AnotherCustomType {
        AnotherCustomType(20u64)
    }
}


#[derive(Clone, Debug)]
pub struct AnotherCustomType(u64);
impl AnotherCustomType {
    pub fn take_primitive_type_and_return_primitive_type(&self, a: u32) -> String {
        format!("Result: {a}")
    }
}

#[binding_wrapper(source = "path/to/_generated_ffi_code/interface.rs")]
mod ffi {

    enum ErrorType {
        Error
    }

    extern "ExceptionTrait" {
        fn reason(&self) -> String;
    }

    extern "Rust" {
        fn return_result_with_dynamic_type(self: &CustomType) -> Result<Arc<Mutex<dyn SomeTrait>>, ErrorType>;
        fn return_another_custom_type(self: &CustomType) -> AnotherCustomType;
        fn take_primitive_type_and_return_primitive_type(self: &AnotherCustomType, a: u32) -> String;
        fn some_trait_method(self: &Arc<Mutex<dyn SomeTrait>>);
    }
}

```

#### 3. Run `cargo build`

This command should generate the glue code and build the static library. The generated code should be visible in directory `./_generated_ffi_code/` (as it was specified in `build.rs`). It contains a code that can be used directly in C++ and Swift applications. It's important to import a static library during the compilation process in both languages.

NOTE: See the `example/` for more information on how to setup the building environment.

#### 4a. Run SWIG in order to generate glue code for other languages:

- `swig -java -c++ -module my_module -outdir _generated_java _generated_ffi_code/ffi_swig.i`
- `swig -csharp -c++ -module my_module -outdir _generated_csharp _generated_ffi_code/ffi_swig.i`
- `swig -python -c++ -module my_module -outdir _generated_python _generated_ffi_code/ffi_swig.i`

#### 4b. Build WebAssembly library (example):

- ````bash
  EMCC_CFLAGS="-s ERROR_ON_UNDEFINED_SYMBOLS=0" \
         cargo build --lib --target wasm32-unknown-emscripten```
  ````
- ````bash
  em++ _generated_javascript/test_wasm.cpp \
         -std=c++20 -g -D WASM \
         -s NO_DISABLE_EXCEPTION_CATCHING \
         -fexceptions \
         -L target/wasm32-unknown-emscripten/debug/ \
         -I ./_generated_ffi_code \
         -l test_interface \
         -l embind \
         -s WASM=1 \
         -o _generated_javascript/test.js```
  ````

## Supported types

**Important note:** Every custom user struct has to implement `Clone` trait.

- `CustomUserType` --> `CustomUserType` - the structure members are not visible beyond the FFI layer - they can be reached indirectly using object's methods.
- `Vec<T>` --> `RustVec<T>` (C++ and Swift) --> `VecT` (other languages)
- `String` --> `String` (C++ and JavaScript) --> `RustString` (other languages)
- `Option<T>` --> `OptionT`
- `Result<T, ErrorType>` --> `ResultT`
- `Arc<Mutex<T>>` --> `SharedMutexT`
- `Arc<Mutex<dyn T>>` --> `SharedMutexT`
- `u8, i8, u16, i16, ...` --> `unsigned char, char, unsigned short ...` (C++)

## Shared Enums

Rust enums are possible to be exposed to other languages by redefining them in `binding_wrapper` module.

**Example:**

```rust
#[binding_wrapper(source = "path/to/_generated_ffi_code/interface.rs")]
mod ffi {
    enum PrimitiveEnum {
        PrimitiveVariantA,
        PrimitiveVariantB,
        PrimitiveVariantC,
    }

    enum DataCarryingEnum {
        UnitVariant,
        Int32Variant(i32),
        StringVariant(String),
        CustomTypeVariant(CustomType),
        NamedTwoFieldsVariant { a: f32, b: u8 },
        UnnamedFieldsVariant(f32, u8, bool),
        ManyOpaqueFields(u32, String, CustomType),
        // String(String) // In case of variant with the same name as other type c++ won't compile due to names conflict. Although, it is possible to support some `rename` attribute.
    }
}
```

Both, primitive enums (with unit fields only) and complex enums are supported. Primitive ones are directly converted to C++ enum classes whereas complex ones are translated into structure with tag and payload. Those structures exposes getter of specific variant - no direct field access possible.

It is important to keep variants in the same order in Rust code and in ffi bridge layer definition since representation must be the same for Rust and other languages.

Complex enum variants may consist of many primitive or opaque types.
If enum variant is a list of named fields, than those names are used in C++ representation (payload is a structure then) whereas in the case of unnamed fields names are generated with sequential integers starting from 0 (e.g. field `_0` has `get_0` getter method).

**Important Note:**
Rust enum definitions (not the ones inside `binding_wrapper` module) must be decorated with the `#[repr(C)]` attribute so the Rust enum and C++ enum class memory layouts match.

```rust
#[repr(C)]
pub enum PrimitiveEnum {
    PrimitiveVariantA,
    PrimitiveVariantB,
    PrimitiveVariantC,
}

#[repr(C)]
#[derive(Clone, Debug)]
pub enum DataCarryingEnum {
    UnitVariant,
    Int32Variant(i32),
    StringVariant(String),
    CustomTypeVariant(CustomType),
    NamedTwoFieldsVariant { a: f32, b: u8 },
    UnnamedFieldsVariant(f32, u8, bool),
    ManyOpaqueFields(u32, String, CustomType),
}
```

Enums can be received as function/methods arguments and can be returned from methods/functions.

## Results -> Exceptions

Functions/methods listed in `extern "Rust"` block that return `std::result::Result` are translated into C++ functions/methods that may throw exceptions. The error type of `Result` must be some Rust enum (primitive or complex) which variants specify possible exceptions.

```rust
#[binding_wrapper(source = "path/to/_generated_ffi_code/interface.rs")]
mod ffi {
    enum PrimitiveEnum {
        PrimitiveVariantA,
        PrimitiveVariantB,
        PrimitiveVariantC,
    }

    extern "Rust" {
        fn function_raising_primitive_exception() -> Result<String, PrimitiveEnum>;
    }
}
```

For each variant a C++ class is generated, deriving from RustBaseException. A C++ function returning string, that may possibly throw one of the following exception classes: `PrimitiveVariantAException`, `PrimitiveVariantBException`, `PrimitiveVariantCException` is generated based on the above ffi module. Data carried by variants is not taken into account during exception classes generation but it matters while defining exceptions common interface.

`RustExceptionBase` is a virtual class deriving from `std::exception` and filled with additional abstract specified by `extern "ExceptionTrait"` block. It also matters for exception handling in javascript.

It is sometimes convenient to attach additional information about an error, besides an exception's class. In order to specify interface, that all rust enums representing any error type in any `Result<_, error_type>` used in `#[binding_wrapper]` module, the `extern "ExceptionTrait"` block can be defined. It should list methods that can be called on every error enum, all of which must not take any arguments except `self`. Return type is arbitrary.

```rust
#[binding_wrapper(source = "path/to/_generated_ffi_code/interface.rs")]
mod ffi {
    ...
    extern "ExceptionTrait" {
        fn code(&self) -> i32;
        fn reason(&self) -> String;
        fn domain(&self) -> Domain;
    }
}
```

If the above block of code is placed in ffi module, every C++ exception may retrieve additional information about context of the error using specified methods. Those methods are purely abstract in `RustBaseException` and calling on a specific exception class is always delegated to Rust enum implementation of the `ExceptionTrait`. It is up to Rust implementation then, to return variant specific value if desired.

**Example**

```rust
impl ExceptionTrait for PrimitiveEnum {
    fn code(&self) -> i32 {
        match self {
            Self::PrimitiveVariantA => 1,
            Self::PrimitiveVariantB => 10,
            Self::PrimitiveVariantC => 100,
        }
    }
    fn reason(&self) -> String {
        match self {
            Self::PrimitiveVariantA => "variant A".to_string(),
            Self::PrimitiveVariantB => "variant B".to_string(),
            Self::PrimitiveVariantC => "variant C".to_string(),
        }
    }
    fn domain(&self) -> Domain {
        Domain::DomainX
    }
}
```

Generated exceptions can be used in C++ for example in the following manner:

```c++
try
{
    String str_with_primitive_exc = function_raising_primitive_exception();
}
catch (const PrimitiveVariantBException &e)
{
    assert(e.code() == 10);
    assert(e.domain() == Domain::DomainX);
    assert(e.reason().to_string() == "variant B");
    assert(e.what() == "PrimitiveVariantBException thrown from Rust");
}
catch (const PrimitiveVariantCException &e)
{
    // do something else
}
catch (const std::exception &e)
{
    // it is possible not to distinguish particular exception classes and catch any using std::exception
}
```

Usage in wasm slightly differs because due to the fact that caught error in the beginning is only a pointer to an exception class. Therefore it requires an additional step to wrap it within `RustException` class to get to its functionalities.

```javascript
try
{
    var str_with_primitive_exc = Module.function_raising_primitive_exception();
}
catch (e)
{
    var exception = new Module.RustException(e);
    assert(exception.exception_class() == Module.ExceptionClass.PrimitiveVariantBException);
    assert(exception.code() == 10);
    assert(exception.domain() == Module.Domain.DomainX);
    assert(exception.reason().to_string() == "variant B");
    assert(exception.what() == "PrimitiveVariantBException thrown from Rust");
}
```

## #[cfg] attribute support

This library provides partial support for cfg attribute to enable conditional compilation.
For example this code will emit `only_windows_function` only when compiled for windows.

```rust
#[binding_wrapper(source = "path/to/_generated_ffi_code/interface.rs")]
mod ffi {
    extern "Rust" {
        #[cfg(target_os = "windows")]
        fn only_windows_function() -> u32
    }
}
```

**Important note:** Not every configuration predicate is supported.

## Testing

In order to run tests one can use the following commands:

- `cargo test` - should work without any special setup
- `./test_script.sh` - this script requires `swig`, `g++`, `emsdk` and `node.js` installed.
