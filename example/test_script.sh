#
# Wildland Project
#
# Copyright © 2022 Golem Foundation,
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


#!/bin/bash

set -e

CC=g++
CSHARP_COMPILER=mcs
JDK_INC_DIR="$(dirname "`readlink -f $(which java)`")"/../include

cd test_interface

#
# Cleanup
#
rm -r _generated_ffi_code \
    _generated_csharp \
    _generated_java \
    _generated_python \
    _generated_javascript || true


#
# Build the library
#
cargo clean
cargo build
cp target/debug/libtest_interface.a ./_generated_ffi_code


#
# Testsuites
#
echo "Test Swift"
cp -r ../platform_testsuites/SwiftTest _generated_ffi_code/
mkdir _generated_ffi_code/SwiftTest/FfiModule/Sources/
mkdir _generated_ffi_code/SwiftTest/FfiModule/Sources/FfiModule
cp _generated_ffi_code/ffi_swift.swift _generated_ffi_code/SwiftTest/FfiModule/Sources/FfiModule/ffi_swift.swift
cp _generated_ffi_code/ffi_swift.h _generated_ffi_code/SwiftTest/CFfiModule/Sources/CFfiModule/ffi_swift.h
echo "@_exported import CFfiModule" >> _generated_ffi_code/SwiftTest/FfiModule/Sources/FfiModule/ffi_swift.swift
cd _generated_ffi_code/SwiftTest/ModuleTest
swift run -Xswiftc -L../../
cd ../../../

echo "Test C++"
cp ../platform_testsuites/test.cpp ./_generated_ffi_code
g++ -std=c++20 -w _generated_ffi_code/test.cpp \
        -I . \
        -I ./_generated_ffi_code \
        -L ./_generated_ffi_code \
        -l test_interface \
        -l dl \
        -l pthread \
        -o ./_generated_ffi_code/test
./_generated_ffi_code/test

echo "Test Python"
mkdir -p _generated_python
swig -python -c++ -w503,509,474,362 -outdir _generated_python -I_generated_ffi_code _generated_ffi_code/ffi_swig.i
${CC} -shared -fPIC --std=c++20 -w \
        _generated_ffi_code/ffi_swig_wrap.cxx \
        -L ./_generated_ffi_code \
        -l test_interface \
        -l pthread \
        -l dl \
        $(python3-config --includes) \
        -I _generated_ffi_code \
        -o _generated_python/_ffi_interface.so
cp ../platform_testsuites/test.py ./_generated_python/
python3 _generated_python/test.py

echo "Test Java"
mkdir -p _generated_java
swig -java -c++ -w503,516 -outdir _generated_java -I_generated_ffi_code _generated_ffi_code/ffi_swig.i
${CC} -fpermissive -shared -fPIC --std=c++20 -w \
        _generated_ffi_code/ffi_swig_wrap.cxx \
        -L ./_generated_ffi_code \
        -l test_interface \
        -l pthread \
        -l dl \
        -I ${JDK_INC_DIR} \
        -I ${JDK_INC_DIR}/linux \
        -I _generated_ffi_code \
        -o _generated_java/libffi_interface.so
cp ../platform_testsuites/test.java ./_generated_java/
javac _generated_java/*.java
java -cp _generated_java -Djava.library.path=./_generated_java main

echo "Test C#"
mkdir -p _generated_csharp
swig -csharp -c++ -w516,503 -outdir _generated_csharp  -I_generated_ffi_code _generated_ffi_code/ffi_swig.i
${CC} -shared -fPIC --std=c++20 -w \
        _generated_ffi_code/ffi_swig_wrap.cxx \
        -I _generated_ffi_code \
        -L ./_generated_ffi_code \
        -l test_interface \
        -l pthread \
        -l dl \
        -o _generated_csharp/libffi_interface.so
cp ../platform_testsuites/test.cs ./_generated_csharp
${CSHARP_COMPILER} -out:_generated_csharp/test.exe _generated_csharp/*.cs
mono _generated_csharp/test.exe

echo "Test JavaScript"
mkdir -p _generated_javascript
cp ../platform_testsuites/test_wasm.cpp _generated_javascript/
/./emsdk/emsdk activate latest
. /emsdk/emsdk_env.sh
EMCC_CFLAGS="-s ERROR_ON_UNDEFINED_SYMBOLS=0" \
        cargo build --lib --target wasm32-unknown-emscripten
em++ _generated_javascript/test_wasm.cpp \
        -std=c++20 -g -D WASM \
        -s NO_DISABLE_EXCEPTION_CATCHING \
        -fexceptions \
        -L target/wasm32-unknown-emscripten/debug/ \
        -I ./_generated_ffi_code \
        -l test_interface \
        -l embind \
        -s WASM=1 \
        -o _generated_javascript/test.js
node ./_generated_javascript/test.js
