# RustyBind test suite

This directory consists of an example code that is used as an integration test suite. By default every new feature is tested and integrated by adding new methods/structures to `test_interface/src/lib.rs` file. This example rust project can be build by `cargo build` command and as a result it should generate C++, Swift and SWIG gluecode in `_generated_ffi_code` directory. There are also a language specific test files in `platform_testsuites` that contains the same version of a test translated to different target languages. This can be further use to test the example rust FFI project in different platforms.

All of the mentioned steps are automated in `test_script.sh` that is also used in the project's CI pipeline.
