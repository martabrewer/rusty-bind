//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

///
/// # Example Rust module that can be used from other platforms.
///
use rusty_bind::binding_wrapper;
use std::sync::{Arc, Mutex};

use extern_crate::AllIgnoredEnum;

// Define `()` type.
type VoidType = ();

pub trait SomeTrait {
    fn some_trait_method(&self, input: u32);
}

#[derive(Clone)]
pub struct NotExposedType(u32);
impl SomeTrait for NotExposedType {
    fn some_trait_method(&self, input: u32) {
        assert!(input == self.0);
    }
}

#[derive(Clone, Debug)]
pub struct CustomType;
impl CustomType {
    pub fn return_static_custom_type(self: &CustomType) -> AnotherCustomType {
        AnotherCustomType(99u32)
    }
    pub fn take_static_custom_type_ref(self: &CustomType, another_custom_type: &AnotherCustomType) {
        another_custom_type.check_inner_value(99u32);
    }
    pub fn take_option_with_shared_dynamic_type(
        self: &CustomType,
        opt: &Option<Arc<Mutex<dyn SomeTrait>>>,
    ) {
        opt.as_ref().unwrap().lock().unwrap().some_trait_method(10);
    }
    #[allow(clippy::too_many_arguments)]
    pub fn take_primitive_type(
        self: &CustomType,
        u1: u8,
        u2: u16,
        u3: u32,
        u4: u64,
        i1: i8,
        i2: i16,
        i3: i32,
        i4: i64,
        b: bool,
    ) {
        assert!(u1 == 1);
        assert!(u2 == 1);
        assert!(u3 == 1);
        assert!(u4 == 1);
        assert!(i1 == 1);
        assert!(i2 == 1);
        assert!(i3 == 1);
        assert!(i4 == 1);
        assert!(b);
    }
    pub fn take_string_return_another_string(self: &CustomType, s: String) -> String {
        assert!(s == "String from a foreign language");
        "String from Rust".to_owned()
    }
    pub fn return_primitive_u8(self: &CustomType) -> u8 {
        255u8
    }
    pub fn return_primitive_i64(self: &CustomType) -> i64 {
        i64::MIN
    }
    pub fn return_option_with_shared_static_type(
        self: &CustomType,
    ) -> Option<Arc<Mutex<AnotherCustomType>>> {
        Some(Arc::new(Mutex::new(AnotherCustomType(99u32))))
    }
    pub fn return_option_with_shared_dynamic_type(
        self: &CustomType,
    ) -> Option<Arc<Mutex<dyn SomeTrait>>> {
        Some(Arc::new(Mutex::new(NotExposedType(10u32))))
    }
    pub fn return_option_with_static_type(self: &CustomType) -> Option<AnotherCustomType> {
        Some(AnotherCustomType(99u32))
    }
    pub fn return_option_with_none(self: &CustomType) -> Option<AnotherCustomType> {
        None
    }
    pub fn return_vector_string(self: &CustomType) -> Vec<String> {
        vec![
            "First String from Rust".to_owned(),
            "Second String from Rust".to_owned(),
        ]
    }
    pub fn return_vector_primitive(self: &CustomType) -> Vec<u8> {
        vec![10, 11]
    }
    pub fn return_vector_custom(self: &CustomType) -> Vec<AnotherCustomType> {
        vec![AnotherCustomType(10), AnotherCustomType(11)]
    }
    pub fn take_vectors(
        self: &CustomType,
        vec_string: Vec<String>,
        vec_u8: Vec<u8>,
        vec_custom: Vec<AnotherCustomType>,
    ) {
        assert!(vec_string.len() == 3);
        assert!(vec_string[0] == "First String from Rust");
        assert!(vec_string[1] == "Second String from Rust");
        assert!(vec_string[2] == "String from a foreign language");

        assert!(vec_u8.len() == 3);
        assert!(vec_u8[0] == 10);
        assert!(vec_u8[1] == 11);
        assert!(vec_u8[2] == 111);

        assert!(vec_custom.len() == 3);
        vec_custom[0].check_inner_value(10);
        vec_custom[1].check_inner_value(11);
        vec_custom[2].check_inner_value(99);
    }
    pub fn take_option_and_arc(
        self: &CustomType,
        opt: Option<AnotherCustomType>,
        arc: Arc<Mutex<AnotherCustomType>>,
    ) {
        opt.unwrap().check_inner_value(99);
        arc.lock().unwrap().check_inner_value(99);
    }
    pub fn enum_variant_c(&self, en: PrimitiveEnum) -> PrimitiveEnum {
        let _en = en;
        PrimitiveEnum::PrimitiveVariantC
    }
    pub fn method_returning_data_carrying_enum_with_many_opaque_fields(&self) -> DataCarryingEnum {
        DataCarryingEnum::ManyOpaqueFields(
            10u32,
            String::from("Hello"),
            CustomType {},
            String::from("Second Hello"),
        )
    }
    pub fn method_raising_exceptions_returns_custom_type_ok(
        &self,
    ) -> Result<CustomType, DataCarryingEnum> {
        Ok(CustomType)
    }
    pub fn method_raising_exceptions_returns_custom_type_string_err(
        &self,
    ) -> Result<CustomType, DataCarryingEnum> {
        Err(DataCarryingEnum::StringVariant(
            "Method exception".to_string(),
        ))
    }
    pub fn take_and_return_complex_generic_types(
        &self,
        opt: Option<Vec<Option<String>>>,
        vec: Vec<Option<Vec<String>>>,
    ) -> Vec<Option<Vec<String>>> {
        assert!(opt.unwrap()[0].as_ref().unwrap() == "First inner string");
        assert!(vec[0].as_ref().unwrap()[0] == "Second inner string");
        vec
    }
}

#[derive(Clone)]
pub struct AnotherCustomType(u32);
impl AnotherCustomType {
    pub fn check_inner_value(&self, input: u32) {
        assert!(input == self.0);
    }
}

pub fn global_function(s: String) -> String {
    assert!(s == "String from a foreign language");
    "String from Rust".to_owned()
}

pub fn should_not_be_present() -> u32 {
    42
}

pub fn should_be_present() -> u32 {
    52
}

pub fn return_custom_type() -> CustomType {
    CustomType {}
}

pub trait CustomTrait {
    fn empty_method(&self);
    fn take_primitives_return_primitives(&self, a: u32, b: u32) -> u32;
    fn take_string_return_string(&self, string: String) -> String;
    fn take_custom_obj_return_custom_obj(&self, a: AnotherCustomType) -> AnotherCustomType;
    fn take_vec_return_vec(
        &self,
        vec_primitive: Vec<u32>,
        vec_custom: Vec<AnotherCustomType>,
    ) -> Vec<AnotherCustomType>;
    fn take_shared_object_and_return(
        &self,
        dynamic_shared_type: Arc<Mutex<dyn SomeTrait>>,
    ) -> Arc<Mutex<dyn SomeTrait>>;
    fn return_ok_result(&self) -> Result<u32, PrimitiveEnum>;
    fn return_err_result(&self) -> Result<u32, PrimitiveEnum>;
    fn return_some_opt(&self) -> Option<String>;
    fn return_none_opt(&self) -> Option<String>;
}

pub fn function_takes_ref_to_trait_object(trait_obj: &dyn CustomTrait) {
    trait_obj.empty_method();
    let integer = trait_obj.take_primitives_return_primitives(11u32, 12u32);
    assert!(integer == 10u32);
    let string = trait_obj.take_string_return_string("String from Rust".to_owned());
    assert!(string == "String from a foreign language");
    let another_custom_type = trait_obj.take_custom_obj_return_custom_obj(AnotherCustomType(99));
    another_custom_type.check_inner_value(99);
    let vec_another_custom_type =
        trait_obj.take_vec_return_vec(vec![10u32], vec![AnotherCustomType(99)]);
    vec_another_custom_type[0].check_inner_value(10);
    vec_another_custom_type[1].check_inner_value(11);
    let shared = trait_obj.take_shared_object_and_return(Arc::new(Mutex::new(NotExposedType(99))));
    assert!(trait_obj.return_ok_result().unwrap() == 10u32);
    assert!(trait_obj.return_err_result().unwrap_err() == PrimitiveEnum::PrimitiveVariantA);
    assert!(trait_obj.return_some_opt().unwrap() == "Some string from the native application");
    assert!(trait_obj.return_none_opt().is_none());
    shared.lock().unwrap().some_trait_method(10);
}

#[derive(Debug, Clone, PartialEq)]
#[repr(C)]
pub enum PrimitiveEnum {
    PrimitiveVariantA,
    PrimitiveVariantB,
    PrimitiveVariantC,
}

#[derive(Debug, Clone)]
#[repr(C)]
pub enum OtherPrimitiveEnum {
    PrimitiveVariantA,
    PrimitiveVariantB,
}

fn function_taking_primitive_enum(en: PrimitiveEnum) {
    let _en = en;
}

fn function_taking_other_primitive_enum(en: OtherPrimitiveEnum) {
    let _en = en;
}

fn function_returning_primitive_enum_variant_b_without_args() -> PrimitiveEnum {
    PrimitiveEnum::PrimitiveVariantB
}

fn function_taking_primitive_enum_and_returning_primitive_enum(en: PrimitiveEnum) -> PrimitiveEnum {
    en
}

// It is important to keep variants in the same order in Rust code and in ffi bridge layer definition
// since representation must be the same for Rust and C++
#[repr(C)]
#[derive(Clone, Debug)]
pub enum DataCarryingEnum {
    UnitVariant,
    Int32Variant(i32),
    StringVariant(String),
    CustomTypeVariant(CustomType),
    NamedTwoFieldsVariant { a: f32, b: u8 },
    UnnamedFieldsVariant(f32, u8, bool),
    ManyOpaqueFields(u32, String, CustomType, String),
    SingleNamedField { some_field: String },
    IgnoredVariantField(String),
}

fn function_returning_data_carrying_enum_with_unit_variant() -> DataCarryingEnum {
    DataCarryingEnum::UnitVariant
}

fn function_returning_data_carrying_enum_with_int32_value_5() -> DataCarryingEnum {
    DataCarryingEnum::Int32Variant(5)
}

fn function_returning_data_carrying_enum_with_string() -> DataCarryingEnum {
    DataCarryingEnum::StringVariant("hello".to_string())
}

fn function_returning_data_carrying_enum_with_custom_type() -> DataCarryingEnum {
    DataCarryingEnum::CustomTypeVariant(CustomType {})
}

fn function_returning_data_carrying_enum_with_named_primitive_fields() -> DataCarryingEnum {
    DataCarryingEnum::NamedTwoFieldsVariant { a: 1.1, b: 2 }
}

fn function_returning_data_carrying_enum_with_unnamed_primitive_fields() -> DataCarryingEnum {
    DataCarryingEnum::UnnamedFieldsVariant(1.1, 2, true)
}

fn function_returning_data_carrying_enum_with_single_named_field() -> DataCarryingEnum {
    DataCarryingEnum::SingleNamedField {
        some_field: "Some field".to_string(),
    }
}

fn function_returning_data_carrying_enum_with_many_opaque_fields() -> DataCarryingEnum {
    DataCarryingEnum::ManyOpaqueFields(
        3,
        String::from("Hello"),
        CustomType {},
        String::from("Second Hello"),
    )
}

// fn function_taking_data_carrying_enum(en: DataCarryingEnum) {} // TODO WILX-184 not supported yet!

fn function_raising_primitive_exception() -> Result<String, PrimitiveEnum> {
    Err(PrimitiveEnum::PrimitiveVariantB)
}

fn function_raising_other_primitive_exception() -> Result<String, OtherPrimitiveEnum> {
    Err(OtherPrimitiveEnum::PrimitiveVariantB)
}

fn function_raising_exceptions_returns_ok() -> Result<String, DataCarryingEnum> {
    Ok("Ok".to_string())
}

fn function_raising_exceptions_returns_int_exception() -> Result<String, DataCarryingEnum> {
    Err(DataCarryingEnum::Int32Variant(420))
}

fn function_raising_exceptions_returns_string_exception() -> Result<String, DataCarryingEnum> {
    Err(DataCarryingEnum::StringVariant(
        "This is exception from rust".to_string(),
    ))
}

fn function_raising_exceptions_returns_vec_of_strings() -> Result<Vec<String>, DataCarryingEnum> {
    Ok(vec![
        "Hello".to_owned(),
        "from".to_owned(),
        "Rust".to_owned(),
    ])
}

fn function_raising_exceptions_returns_optional_string() -> Result<Option<String>, DataCarryingEnum>
{
    Ok(Some("Hello".to_owned()))
}

fn function_raising_exceptions_returns_ignored_variant_exc(
) -> Result<Option<String>, DataCarryingEnum> {
    Err(DataCarryingEnum::IgnoredVariantField(
        "Some string".to_string(),
    ))
}

fn function_raising_exceptions_returns_void() -> Result<(), PrimitiveEnum> {
    Ok(())
}

fn function_raising_exceptions_returns_custom_type() -> Result<CustomType, PrimitiveEnum> {
    Ok(CustomType {})
}

#[repr(C)]
#[derive(Clone, Debug)]
pub enum Domain {
    DomainX,
    DomainY,
}

pub trait ExceptionTrait {
    fn code(&self) -> i32;
    fn reason(&self) -> String;
    fn domain(&self) -> Domain;
}

impl ExceptionTrait for PrimitiveEnum {
    fn code(&self) -> i32 {
        match self {
            Self::PrimitiveVariantA => 1,
            Self::PrimitiveVariantB => 10,
            Self::PrimitiveVariantC => 100,
        }
    }
    fn reason(&self) -> String {
        match self {
            Self::PrimitiveVariantA => "variant A".to_string(),
            Self::PrimitiveVariantB => "variant B".to_string(),
            Self::PrimitiveVariantC => "variant C".to_string(),
        }
    }
    fn domain(&self) -> Domain {
        Domain::DomainX
    }
}

impl ExceptionTrait for OtherPrimitiveEnum {
    fn code(&self) -> i32 {
        420
    }
    fn reason(&self) -> String {
        match self {
            Self::PrimitiveVariantA => "variant A".to_string(),
            Self::PrimitiveVariantB => "variant B".to_string(),
        }
    }
    fn domain(&self) -> Domain {
        Domain::DomainX
    }
}

impl ExceptionTrait for DataCarryingEnum {
    fn code(&self) -> i32 {
        2
    }

    fn reason(&self) -> String {
        format!("{self:?}")
    }
    fn domain(&self) -> Domain {
        Domain::DomainY
    }
}

pub trait AsyncHandler: Sync + Send {
    fn callback(&self);
}

pub fn take_async_handler(handler: &dyn AsyncHandler) {
    handler.callback();
}

#[binding_wrapper(source = "../_generated_ffi_code/interface.rs")]
mod ffi {
    enum Domain {
        DomainX,
        DomainY,
    }

    enum PrimitiveEnum {
        PrimitiveVariantA,
        PrimitiveVariantB,
        PrimitiveVariantC,
    }

    enum OtherPrimitiveEnum {
        PrimitiveVariantA,
        PrimitiveVariantB,
    }

    // It is important to keep variants in the same order in Rust code and in ffi bridge layer definition
    // since representation must be the same for Rust and other languages (via C ABI)
    enum DataCarryingEnum {
        UnitVariant,
        Int32Variant(i32),
        StringVariant(String),
        CustomTypeVariant(CustomType),
        NamedTwoFieldsVariant { a: f32, b: u8 },
        UnnamedFieldsVariant(f32, u8, bool),
        ManyOpaqueFields(u32, String, CustomType, String),
        SingleNamedField { some_field: String },
        // Variant data may be ignore so the getters are not generated, while the exception class is.
        IgnoredVariantField(_),
        // String(String) // In case of variant with the same name as other type c++ won't compile due to names conflict. Although, it is possible to support some `rename` attribute.
    }

    // impl block is not generated for enums that have all variants ignored
    // useful when enum is declared in another crate
    enum AllIgnoredEnum {
        Typed(_),
        Ignored,
    }

    extern "Traits" {
        type CustomTrait;
        fn empty_method(self: &dyn CustomTrait);
        fn take_primitives_return_primitives(self: &dyn CustomTrait, a: u32, b: u32) -> u32;
        fn take_string_return_string(self: &dyn CustomTrait, string: String) -> String;
        fn take_custom_obj_return_custom_obj(
            self: &dyn CustomTrait,
            a: AnotherCustomType,
        ) -> AnotherCustomType;
        fn take_vec_return_vec(
            self: &dyn CustomTrait,
            vec_primitive: Vec<u32>,
            vec_custom: Vec<AnotherCustomType>,
        ) -> Vec<AnotherCustomType>;
        fn take_shared_object_and_return(
            self: &dyn CustomTrait,
            dynamic_shared_type: Arc<Mutex<dyn SomeTrait>>,
        ) -> Arc<Mutex<dyn SomeTrait>>;
        fn return_ok_result(self: &dyn CustomTrait) -> Result<u32, PrimitiveEnum>;
        fn return_err_result(self: &dyn CustomTrait) -> Result<u32, PrimitiveEnum>;
        fn return_some_opt(self: &dyn CustomTrait) -> Option<String>;
        fn return_none_opt(self: &dyn CustomTrait) -> Option<String>;

        fn callback(self: &dyn AsyncHandler);
    }

    extern "ExceptionTrait" {
        fn code(&self) -> i32;
        fn reason(&self) -> String;
        fn domain(&self) -> Domain;
    }

    extern "Rust" {
        fn take_async_handler(handler: &dyn AsyncHandler);

        fn function_raising_exceptions_returns_ok() -> Result<String, DataCarryingEnum>;
        fn function_raising_exceptions_returns_int_exception() -> Result<String, DataCarryingEnum>;
        fn function_raising_exceptions_returns_string_exception() -> Result<String, DataCarryingEnum>;
        fn function_raising_exceptions_returns_vec_of_strings(
        ) -> Result<Vec<String>, DataCarryingEnum>;
        fn function_raising_exceptions_returns_optional_string(
        ) -> Result<Option<String>, DataCarryingEnum>;
        fn function_raising_exceptions_returns_ignored_variant_exc(
        ) -> Result<Option<String>, DataCarryingEnum>;
        fn method_raising_exceptions_returns_custom_type_ok(
            self: &CustomType,
        ) -> Result<CustomType, DataCarryingEnum>;
        fn method_raising_exceptions_returns_custom_type_string_err(
            self: &CustomType,
        ) -> Result<CustomType, DataCarryingEnum>;

        fn function_raising_primitive_exception() -> Result<String, PrimitiveEnum>;
        fn function_raising_other_primitive_exception() -> Result<String, OtherPrimitiveEnum>;

        fn function_raising_exceptions_returns_void() -> Result<VoidType, PrimitiveEnum>;
        fn function_raising_exceptions_returns_custom_type() -> Result<CustomType, PrimitiveEnum>;

        fn function_taking_primitive_enum(en: PrimitiveEnum);
        fn function_taking_other_primitive_enum(en: OtherPrimitiveEnum);
        fn function_returning_primitive_enum_variant_b_without_args() -> PrimitiveEnum;
        fn function_taking_primitive_enum_and_returning_primitive_enum(
            en: PrimitiveEnum,
        ) -> PrimitiveEnum;
        fn enum_variant_c(self: &CustomType, en: PrimitiveEnum) -> PrimitiveEnum;

        fn function_returning_data_carrying_enum_with_unit_variant() -> DataCarryingEnum;
        fn function_returning_data_carrying_enum_with_int32_value_5() -> DataCarryingEnum;
        fn function_returning_data_carrying_enum_with_string() -> DataCarryingEnum;
        fn function_returning_data_carrying_enum_with_custom_type() -> DataCarryingEnum;
        fn function_returning_data_carrying_enum_with_named_primitive_fields() -> DataCarryingEnum;
        fn function_returning_data_carrying_enum_with_unnamed_primitive_fields() -> DataCarryingEnum;
        fn function_returning_data_carrying_enum_with_many_opaque_fields() -> DataCarryingEnum;
        fn method_returning_data_carrying_enum_with_many_opaque_fields(
            self: &CustomType,
        ) -> DataCarryingEnum;
        fn function_returning_data_carrying_enum_with_single_named_field() -> DataCarryingEnum;
        // fn function_taking_data_carrying_enum(en: DataCarryingEnum); // TODO WILX-184 receiving complex enums is not supported yet

        fn function_takes_ref_to_trait_object(trait_obj: &dyn CustomTrait);
        fn return_custom_type() -> CustomType;

        fn check_inner_value(self: &AnotherCustomType, input: u32);
        fn check_inner_value(self: &Arc<Mutex<AnotherCustomType>>, input: u32);
        fn some_trait_method(self: &Arc<Mutex<dyn SomeTrait>>, input: u32);

        type VoidType;
        fn return_static_custom_type(self: &CustomType) -> AnotherCustomType;
        fn take_static_custom_type_ref(self: &CustomType, another_custom_type: &AnotherCustomType);
        fn take_option_with_shared_dynamic_type(
            self: &CustomType,
            opt: &Option<Arc<Mutex<dyn SomeTrait>>>,
        );
        fn take_primitive_type(
            self: &CustomType,
            u1: u8,
            u2: u16,
            u3: u32,
            u4: u64,
            i1: i8,
            i2: i16,
            i3: i32,
            i4: i64,
            b: bool,
        );
        fn take_string_return_another_string(self: &CustomType, s: String) -> String;
        fn return_primitive_u8(self: &CustomType) -> u8;
        fn return_primitive_i64(self: &CustomType) -> i64;
        fn return_option_with_shared_static_type(
            self: &CustomType,
        ) -> Option<Arc<Mutex<AnotherCustomType>>>;
        fn return_option_with_shared_dynamic_type(
            self: &CustomType,
        ) -> Option<Arc<Mutex<dyn SomeTrait>>>;
        fn return_option_with_static_type(self: &CustomType) -> Option<AnotherCustomType>;
        fn return_option_with_none(self: &CustomType) -> Option<AnotherCustomType>;
        fn return_vector_string(self: &CustomType) -> Vec<String>;
        fn return_vector_primitive(self: &CustomType) -> Vec<u8>;
        fn return_vector_custom(self: &CustomType) -> Vec<AnotherCustomType>;
        fn take_vectors(
            self: &CustomType,
            vec_string: Vec<String>,
            vec_u8: Vec<u8>,
            vec_custom: Vec<AnotherCustomType>,
        );
        fn take_option_and_arc(
            self: &CustomType,
            opt: Option<AnotherCustomType>,
            arc: Arc<Mutex<AnotherCustomType>>,
        );
        fn take_and_return_complex_generic_types(
            self: &CustomType,
            opt: Option<Vec<Option<String>>>,
            vec: Vec<Option<Vec<String>>>,
        ) -> Vec<Option<Vec<String>>>;

        fn global_function(s: String) -> String;

        #[cfg(target_arch = "arm")]
        fn should_not_be_present() -> u32;

        #[cfg(not(target_arch = "arm"))]
        fn should_be_present() -> u32;
    }
}
