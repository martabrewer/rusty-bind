//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/// This piece of code is compiled and run by rust cargo before
/// the actual lib/app compilation process. It is required to run
/// rusty_bind aside in order to generate the glue code for other
/// languages. The results of this example script consist of
/// C++ and Swift code placed in  `_generated_ffi_code` directory.
///
/// The generated Swift code can be used directly in any Swift
/// application/library. The C++ code can be used as:
/// * a C++ header in any C++ application/library,
/// * a gluecode for Emscripten in order to compile WebAssembly application,
/// * an input for SWIG (recommended ver >= 4.0) in order to generate glue code
///   for Java/C#/Python etc.
///
fn main() {
    use rusty_bind_build::parse_ffi_module;
    parse_ffi_module("src/lib.rs", "./_generated_ffi_code/").unwrap_or_else(|e| println!("{e}"));
}
