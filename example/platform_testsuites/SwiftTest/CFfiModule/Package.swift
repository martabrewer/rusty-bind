// swift-tools-version: 5.5
import PackageDescription

let package = Package(
    name: "CFfiModule",
    products: [
        .library(name: "CFfiModule", targets: ["CFfiModule"]),
    ],
    targets: [
        .systemLibrary(name: "CFfiModule"),
    ]
)
