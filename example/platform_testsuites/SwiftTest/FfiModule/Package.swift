// swift-tools-version: 5.5
import PackageDescription

let package = Package(
    name: "FfiModule",
    products: [
        .library(name: "FfiModule", targets: ["FfiModule"]),
    ],
    dependencies: [
        .package(path: "../CFfiModule"),
    ],
    targets: [
        .target(
            name: "FfiModule",
            dependencies: ["CFfiModule"]),
    ]
)
