
//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


import Foundation
import FfiModule

print("Swift FFI Test Suite")


class TraitImplementation : CustomTrait {
    public override func emptyMethod() {
    }

    public override func takePrimitivesReturnPrimitives(_ a: UInt32, _ b: UInt32) -> UInt32 {
        if(a != 11) {
            fatalError("Primitive type value doesn't match in exposed Rust trait (a)")
        }
        if(b != 12) {
            fatalError("Primitive type value doesn't match in exposed Rust trait (b)")
        }
        return 10
    }

    public override func takeStringReturnString(_ string_: RustString) -> RustString {
        if(string_.toString() != "String from Rust") {
            fatalError("Passed string doesn't match")
        }
        return RustString("String from a foreign language")
    }

    public override func takeCustomObjReturnCustomObj(_ another_custom: AnotherCustomType) -> AnotherCustomType {
        another_custom.checkInnerValue(99)
        return another_custom
    }

    public override func takeVecReturnVec(_ vec_primitive: RustVec<UInt32>, _ vec_custom: RustVec<AnotherCustomType>) -> RustVec<AnotherCustomType> {
        if(vec_primitive[0] != 10) {
            fatalError("Returned first u8 from vector doesn't match")
        }
        vec_custom[0].checkInnerValue(99);
        let custom_type = returnCustomType()
        return custom_type.returnVectorCustom()
    }

    public override func takeSharedObjectAndReturn(_ dynamic_shared_type: SharedMutexSomeTrait) -> SharedMutexSomeTrait {
        dynamic_shared_type.someTraitMethod(99)
        let custom_type = returnCustomType()
        return custom_type.returnOptionWithSharedDynamicType().swiftOptional()!
    }

    public override func returnOkResult() -> u32ResultWithPrimitiveEnum {
        u32ResultWithPrimitiveEnum.from_ok(10)
    }

    public override func returnErrResult() -> u32ResultWithPrimitiveEnum {
        u32ResultWithPrimitiveEnum.from_err(PrimitiveEnum_PrimitiveVariantA)
    }

    public override func returnSomeOpt() -> RustOptional<RustString> {
        Optional.some(RustString("Some string from the native application")).toRustOptional()
    }

    public override func returnNoneOpt() -> RustOptional<RustString> {
        Optional.none.toRustOptional()
    }
}

// Assert that Rust traits can be implemented by native languages.
let a = TraitImplementation()
functionTakesRefToTraitObject(a)


// Assert that a global function can return a custom user object.
let custom_type: CustomType = returnCustomType()

// Assert that an object's method can return a custom user object.
let another_custom_type: AnotherCustomType = custom_type.returnStaticCustomType()
another_custom_type.checkInnerValue(99)

// Assert that a function can take a reference to some custom user object.
custom_type.takeStaticCustomTypeRef(another_custom_type)

// Assert that a function can take primitive objects by value.
custom_type.takePrimitiveType(1, 1, 1, 1, 1, 1, 1, 1, true)

// Assert that a function can take and return strings by value.
assert("String from Rust" == custom_type.takeStringReturnAnotherString(RustString("String from a foreign language")).toString())

// Assert that a function can return a primitive object by value
assert(custom_type.returnPrimitiveU8() == 255)

// Assert that a function can return an Int64 object by value
assert(custom_type.returnPrimitiveI64() == -9223372036854775808)

// Assert that a function can return an option object with a custom user object implementing some rust trait
// wrapped in a reference counted mutex.
custom_type.returnOptionWithSharedDynamicType().swiftOptional()!.someTraitMethod(10)

// Assert that a function can return an option object with a custom user object
// wrapped in a reference counted mutex.
custom_type.returnOptionWithStaticType().swiftOptional()!.checkInnerValue(99)

// Assert that a function can return an option object without any object wrapped inside.
assert(custom_type.returnOptionWithNone().swiftOptional() == nil)

// Assert that a function can take and return vectors by value.
let vec_string: RustVec<RustString> = custom_type.returnVectorString()
assert(vec_string[0].toString() == "First String from Rust")
assert(vec_string[1].toString() == "Second String from Rust")
let vec_primitive: RustVec<UInt8> = custom_type.returnVectorPrimitive()
assert(vec_primitive[0] == 10)
assert(vec_primitive[1] == 11)
let vec_custom: RustVec<AnotherCustomType> = custom_type.returnVectorCustom()
vec_custom[0].checkInnerValue(10)
vec_custom[1].checkInnerValue(11)

vec_string.push(RustString("String from a foreign language"));
vec_primitive.push(111);
vec_custom.push(another_custom_type);
custom_type.takeVectors(
    RustVec(
        [RustString("First String from Rust"),
         RustString("Second String from Rust"),
         RustString("String from a foreign language")
        ]),
    RustVec([10, 11, 111].makeIterator()),
    vec_custom
)

// Assert that a function can return option object with a custom user object
// wrapped in a reference counted mutex.
custom_type.returnOptionWithSharedStaticType().swiftOptional()!.checkInnerValue(99);

// Assert that function can take results and optionals with shared dynamic
// objects within.
custom_type.takeOptionWithSharedDynamicType(
    custom_type.returnOptionWithSharedDynamicType().swiftOptional().toRustOptional()
);

// Assert that a global function can take and return strings by value.
assert(globalFunction(RustString("String from a foreign language")).toString() == "String from Rust")

// Assert that a function can take results, options and shared objects by reference.
custom_type.takeOptionAndArc(
    custom_type.returnOptionWithStaticType(),
    custom_type.returnOptionWithSharedStaticType().swiftOptional()!
)

// var vec_with_opt_string_inside = RustVec<RustOptional<RustString>>([Optional.some(RustString("First inner string")).toRustOptional()])
// RustOptional<RustString>.createNewRustVec()
// var vec_with_string = RustVec<RustString>([RustString("Second inner string")])
// var vec_with_opt_with_vec_inside = RustVec<RustOptional<RustVec<RustString>>>([vec_with_string.toRustOptional()])
// assert(custom_type.takeAndReturnComplexGenericTypes(
//             vec_with_opt_string_inside.toRustOptional(),
//             vec_with_opt_with_vec_inside
//         )[0].swiftOptional()![0].swiftOptional()! == "Second inner string")

func exceptionsTests() throws {
    // no exceptions
    let str_with_exc = try functionRaisingExceptionsReturnsOk();
    assert(str_with_exc.toString() == "Ok");

    // exceptions as primitive enum
    do {
        let _: RustString = try functionRaisingPrimitiveException()
        fatalError("Function functionRaisingPrimitiveException should throw an exception")
    }
    catch let err as PrimitiveEnum_PrimitiveVariantBException {
        assert(err.code() == 10)
        assert(err.domain() == Domain_DomainX)
        assert(err.reason().toString() == "variant B")
    }
    catch {
        fatalError("Unhandled error")
    }

    // thrown exception with primitive inside
    do {
        let _ = try functionRaisingExceptionsReturnsIntException()
        fatalError("Function functionRaisingExceptionsReturnsIntException should throw an exception")
    }
    catch let err as DataCarryingEnum_Int32VariantException {
        assert(err.code() == 2);
        assert(err.domain() == Domain_DomainY);
        assert(err.reason().toString() == "Int32Variant(420)")
    }
    catch {
        fatalError("Unhandled error")
    }

    // thrown exception with opaque type
    do {
        let _ = try functionRaisingExceptionsReturnsStringException();
        assert(false);
    }
    catch let err as DataCarryingEnum_StringVariantException {
        assert(err.code() == 2);
        assert(err.domain() == Domain_DomainY);
        assert(err.reason().toString() == "StringVariant(\"This is exception from rust\")");
    }

    // Ok variant with vector
    let vec_str = try functionRaisingExceptionsReturnsVecOfStrings();
    assert(vec_str[0].toString() == "Hello");
    assert(vec_str[1].toString() == "from");
    assert(vec_str[2].toString() == "Rust");

    // Ok variant with optional
    let option_str = try functionRaisingExceptionsReturnsOptionalString();
    assert(option_str.swiftOptional()!.toString() == "Hello");

    // thrown exception with ignored field
    do {
        let _ = try functionRaisingExceptionsReturnsIgnoredVariantExc();
        assert(false);
    }
    catch let err as DataCarryingEnum_IgnoredVariantFieldException {
        assert(err.code() == 2);
        assert(err.domain() == Domain_DomainY);
        assert(err.reason().toString() == "IgnoredVariantField(\"Some string\")");
    }

    // void type inside result
    let _ = try functionRaisingExceptionsReturnsVoid()

    // custom type inside result
    let ct: CustomType = try functionRaisingExceptionsReturnsCustomType()

    // method with ok result
    let _ = try ct.methodRaisingExceptionsReturnsCustomTypeOk();

    // method rasing exception
    do {
        let _ = try ct.methodRaisingExceptionsReturnsCustomTypeStringErr();
        assert(false);
    }
    catch let err as DataCarryingEnum_StringVariantException {
        assert(err.code() == 2);
        assert(err.domain() == Domain_DomainY);
        assert(err.reason().toString() == "StringVariant(\"Method exception\")");
    }
}

try exceptionsTests()

func enumTests() {
    let custom_type: CustomType = returnCustomType()

    // Assert taking primitive enums as arguments and returning them from function/method.
    let some_enum: PrimitiveEnum = functionReturningPrimitiveEnumVariantBWithoutArgs()
    let some_other_enum: PrimitiveEnum = functionTakingPrimitiveEnumAndReturningPrimitiveEnum(some_enum)
    assert(some_other_enum == PrimitiveEnum_PrimitiveVariantB)

    functionTakingPrimitiveEnum(some_other_enum)
    let enum_from_method: PrimitiveEnum = custom_type.enumVariantC(some_other_enum)
    assert(enum_from_method == PrimitiveEnum_PrimitiveVariantC)

    // unit variant
    let some_data_carrying_enum = functionReturningDataCarryingEnumWithUnitVariant()
    assert(some_data_carrying_enum.getTag() == DataCarryingEnumTag_UnitVariant)

    // int variant
    let int_dce = functionReturningDataCarryingEnumWithInt32Value5()
    assert(int_dce.getTag() == DataCarryingEnumTag_Int32Variant)
    assert(int_dce.getInt32Variant() == 5)

    // string variant
    let string_dce = functionReturningDataCarryingEnumWithString()
    assert(string_dce.getTag() == DataCarryingEnumTag_StringVariant)
    assert(string_dce.getStringVariant().toString() == "hello")

    // custom type variant
    let custom_dce = functionReturningDataCarryingEnumWithCustomType()
    assert(custom_dce.getTag() == DataCarryingEnumTag_CustomTypeVariant)
    assert(custom_dce.getCustomTypeVariant().returnPrimitiveU8() == 255)

    // many unnamed fields variant
    let unnamed_dce = functionReturningDataCarryingEnumWithUnnamedPrimitiveFields()
    assert(unnamed_dce.getTag() == DataCarryingEnumTag_UnnamedFieldsVariant)
    assert(unnamed_dce.getUnnamedFieldsVariant().get0() == 1.1)
    assert(unnamed_dce.getUnnamedFieldsVariant().get1() == 2)
    assert(unnamed_dce.getUnnamedFieldsVariant().get2() == true)

    // many named fields variant
    let named_dce = functionReturningDataCarryingEnumWithNamedPrimitiveFields()
    assert(named_dce.getTag() == DataCarryingEnumTag_NamedTwoFieldsVariant)
    assert(named_dce.getNamedTwoFieldsVariant().getA() == 1.1)
    assert(named_dce.getNamedTwoFieldsVariant().getB() == 2)

    // many opaque types
    let multiple_opaque_fields_dce = functionReturningDataCarryingEnumWithManyOpaqueFields();
    assert(multiple_opaque_fields_dce.getTag() == DataCarryingEnumTag_ManyOpaqueFields);
    assert(multiple_opaque_fields_dce.getManyOpaqueFields().get0() == 3);
    assert(multiple_opaque_fields_dce.getManyOpaqueFields().get1().toString() == "Hello");
    assert(multiple_opaque_fields_dce.getManyOpaqueFields().get2().returnPrimitiveU8() == 255);
    assert(multiple_opaque_fields_dce.getManyOpaqueFields().get3().toString() == "Second Hello");
}

enumTests()
