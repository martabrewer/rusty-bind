#
# Wildland Project
#
# Copyright © 2022 Golem Foundation,
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import unittest
import ffi_interface

print("Python FFI Test Suite")


class TraitImplementation(ffi_interface.CustomTrait):
    def __init__(self):
        ffi_interface.CustomTrait.__init__(self)

    def empty_method(self):
        pass

    def take_primitives_return_primitives(self, a, b):
        assert a == 11
        assert b == 12
        return 10

    def take_string_return_string(self, string):
        if "String from Rust" != string.to_string():
            raise Exception("Passed string doesn't match")
        return ffi_interface.RustString("String from a foreign language")

    def take_custom_obj_return_custom_obj(self, another_custom):
        another_custom.check_inner_value(99)
        return another_custom

    def take_vec_return_vec(self, vec_primitive, vec_custom):
        if vec_primitive.at(0).unwrap() != 10:
            raise Exception("Returned first u8 from vector doesn't match")
        vec_custom.at(0).unwrap().check_inner_value(99)
        custom_type = ffi_interface.return_custom_type()
        return custom_type.return_vector_custom()

    def take_shared_object_and_return(self, dynamic_shared_type):
        dynamic_shared_type.some_trait_method(99)
        custom_type = ffi_interface.return_custom_type()
        return custom_type.return_option_with_shared_dynamic_type().unwrap()

    def return_ok_result(self):
        return ffi_interface.u32ResultWithPrimitiveEnum.from_ok(10)

    def return_err_result(self):
        return ffi_interface.u32ResultWithPrimitiveEnum.from_err(ffi_interface.PrimitiveEnum_PrimitiveVariantA)

    def return_some_opt(self):
        return ffi_interface.OptionalRustString(ffi_interface.RustString("Some string from the native application"))

    def return_none_opt(self):
        return ffi_interface.OptionalRustString()


class TestAdminManager(unittest.TestCase):
    def test_ffi(self):
        # Assert that Rust traits can be implemented by native languages.
        a = TraitImplementation()
        ffi_interface.function_takes_ref_to_trait_object(a)

        # Assert that a global function can return a custom user object.
        custom_type = ffi_interface.return_custom_type()

        # Assert that an object's method can return a custom user object.
        another_custom_type = custom_type.return_static_custom_type()
        another_custom_type.check_inner_value(99)

        # Assert that a function can take a reference to some custom user object.
        custom_type.take_static_custom_type_ref(another_custom_type)

        # Assert that a function can take primitive objects by value.
        custom_type.take_primitive_type(1, 1, 1, 1, 1, 1, 1, 1, True)

        # Assert that a function can take and return strings by value.
        if "String from Rust" != custom_type.take_string_return_another_string(ffi_interface.RustString("String from a foreign language")).to_string():
            raise Exception("Returned string doesn't match")

        # Assert that a function can return a primitive object by value
        if custom_type.return_primitive_u8() != 255:
            raise Exception("Returned u8 value doesn't match")

        # Assert that a function can return an Int64 object by value
        if custom_type.return_primitive_i64() != -9223372036854775808:
            raise Exception("Returned u8 value doesn't match")

        # Assert that a function can return an option object with a custom user object implementing some rust trait
        # wrapped in a reference counted mutex.
        custom_type.return_option_with_shared_dynamic_type().unwrap().some_trait_method(10)

        # Assert that a function can return an option object with a custom user object
        # wrapped in a reference counted mutex.
        custom_type.return_option_with_static_type().unwrap().check_inner_value(99)

        # Assert that a function can return an option object without any object wrapped inside.
        if custom_type.return_option_with_none().is_some():
            raise Exception("None option should be present")

        # Assert that a function can take and return vectors by value.
        vec_string = custom_type.return_vector_string()
        if vec_string.at(0).unwrap().to_string() != "First String from Rust":
            raise Exception("Returned first string from vector doesn't match")
        if vec_string.at(1).unwrap().to_string() != "Second String from Rust":
            raise Exception("Returned second string from vector doesn't match")
        vec_primitive = custom_type.return_vector_primitive()
        if vec_primitive.at(0).unwrap() != 10:
            raise Exception("Returned first u8 from vector doesn't match")
        if vec_primitive.at(1).unwrap() != 11:
            raise Exception("Returned second u8 from vector doesn't match")
        vec_custom = custom_type.return_vector_custom()
        vec_custom.at(0).unwrap().check_inner_value(10)
        vec_custom.at(1).unwrap().check_inner_value(11)
        vec_string.push(ffi_interface.RustString(
            "String from a foreign language"))
        vec_primitive.push(111)
        vec_custom.push(another_custom_type)
        custom_type.take_vectors(
            vec_string,
            vec_primitive,
            vec_custom
        )

        # Assert that a function can return option object with a custom user object
        # wrapped in a reference counted mutex.
        custom_type.return_option_with_shared_static_type().unwrap().check_inner_value(99)

        # Assert that function can take results and optionals with shared dynamic
        # objects within.
        custom_type.take_option_with_shared_dynamic_type(
            custom_type.return_option_with_shared_dynamic_type()
        )

        # Assert that a global function can take and return strings by value.
        if ffi_interface.global_function(ffi_interface.RustString("String from a foreign language")).to_string() != "String from Rust":
            raise Exception("Returned string doesn't match")

        # Assert that a function can take results, options and shared objects by reference.
        custom_type.take_option_and_arc(
            custom_type.return_option_with_static_type(),
            custom_type.return_option_with_shared_static_type().unwrap()
        )

        # Assert taking primitive enums as arguments and returning them from function/method.
        some_enum = ffi_interface.function_returning_primitive_enum_variant_b_without_args()
        some_other_enum = ffi_interface.function_taking_primitive_enum_and_returning_primitive_enum(
            some_enum)
        if some_other_enum != ffi_interface.PrimitiveEnum_PrimitiveVariantB:
            raise Exception(
                "Returned wrong primitive enum variant from global function")
        ffi_interface.function_taking_primitive_enum(some_other_enum)
        enum_from_method = custom_type.enum_variant_c(some_other_enum)
        if enum_from_method != ffi_interface.PrimitiveEnum_PrimitiveVariantC:
            raise Exception(
                "Returned wrong primitive enum variant from method")

        # Data enum tests
        unit_dce = ffi_interface.function_returning_data_carrying_enum_with_unit_variant()
        if unit_dce.get_tag() != ffi_interface.DataCarryingEnumTag_UnitVariant:
            raise Exception("Unit variant didn't match")

        int_dce = ffi_interface.function_returning_data_carrying_enum_with_int32_value_5()
        if int_dce.get_tag() != ffi_interface.DataCarryingEnumTag_Int32Variant \
                or int_dce.get_int_32_variant() != 5:
            raise Exception("int variant didn't match")

        string_dce = ffi_interface.function_returning_data_carrying_enum_with_string()
        if string_dce.get_tag() != ffi_interface.DataCarryingEnumTag_StringVariant \
                or string_dce.get_string_variant().to_string() != "hello":
            raise Exception("string variant didn't match")

        unnamed_primitives_dce = ffi_interface.function_returning_data_carrying_enum_with_unnamed_primitive_fields()
        if unnamed_primitives_dce.get_tag() != ffi_interface.DataCarryingEnumTag_UnnamedFieldsVariant \
                or unnamed_primitives_dce.get_unnamed_fields_variant().get_0() == 1.1 \
                or unnamed_primitives_dce.get_unnamed_fields_variant().get_1() != 2 \
                or not unnamed_primitives_dce.get_unnamed_fields_variant().get_2():
            raise Exception(
                "Enum with many primitive fields variant didn't match")

        opaques_dce = ffi_interface.function_returning_data_carrying_enum_with_many_opaque_fields()
        if opaques_dce.get_tag() != ffi_interface.DataCarryingEnumTag_ManyOpaqueFields \
                or opaques_dce.get_many_opaque_fields().get_0() != 3 \
                or opaques_dce.get_many_opaque_fields().get_1().to_string() != "Hello" \
                or opaques_dce.get_many_opaque_fields().get_2().return_primitive_u8() != 255 \
                or opaques_dce.get_many_opaque_fields().get_3().to_string() != "Second Hello":
            raise Exception(
                "Enum with many opaque fields variant didn't match")

        opaques_dce_2 = custom_type.method_returning_data_carrying_enum_with_many_opaque_fields()
        if opaques_dce_2.get_tag() != ffi_interface.DataCarryingEnumTag_ManyOpaqueFields \
                or opaques_dce_2.get_many_opaque_fields().get_0() != 10 \
                or opaques_dce_2.get_many_opaque_fields().get_1().to_string() != "Hello" \
                or opaques_dce_2.get_many_opaque_fields().get_2().return_primitive_u8() != 255 \
                or opaques_dce_2.get_many_opaque_fields().get_3().to_string() != "Second Hello":
            raise Exception(
                "Enum with many opaque fields variant didn't match")
        # TODO WILX-203 exceptions

        vec_with_opt_string_inside = ffi_interface.VecOptionalRustString()
        vec_with_opt_string_inside.push(ffi_interface.OptionalRustString(ffi_interface.RustString("First inner string")))
        vec_with_string = ffi_interface.VecRustString()
        vec_with_string.push(ffi_interface.RustString("Second inner string"))
        vec_with_opt_with_vec_inside = ffi_interface.VecOptionalVecRustString()
        vec_with_opt_with_vec_inside.push(ffi_interface.OptionalVecRustString(vec_with_string))
        if custom_type.take_and_return_complex_generic_types(
                    ffi_interface.OptionalVecOptionalRustString(vec_with_opt_string_inside),
                    vec_with_opt_with_vec_inside
            ) \
            .at(0) \
            .unwrap() \
            .unwrap() \
            .at(0) \
            .unwrap() \
            .to_string() != "Second inner string":
            raise Exception("Complex generic type error - received string doesn't match")

        if hasattr(ffi_interface, 'should_not_be_present'):
            raise Exception("cfg error - function wasn't filtered out")

        if not hasattr(ffi_interface, 'should_be_present'):
            raise Exception("cfg error - function was wrongly filtered out")

if __name__ == '__main__':
    unittest.main()
