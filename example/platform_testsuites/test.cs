//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


using System;

namespace Main
{
    class TraitImplementation : CustomTrait
    {
        public override void empty_method()
        {
        }

        public override uint take_primitives_return_primitives(uint a, uint b)
        {
            if (a != 11)
            {
                throw new Exception("Primitive type value doesn't match in exposed Rust trait (a)");
            }
            if (b != 12)
            {
                throw new Exception("Primitive type value doesn't match in exposed Rust trait (b)");
            }
            return 10;
        }

        public override RustString take_string_return_string(RustString string_)
        {
            if (string_.to_string() != "String from Rust")
            {
                throw new Exception("Passed string doesn't match");
            }
            return new RustString("String from a foreign language");
        }

        public override AnotherCustomType take_custom_obj_return_custom_obj(AnotherCustomType another_custom)
        {
            another_custom.check_inner_value(99);
            return another_custom;
        }

        public override VecAnotherCustomType take_vec_return_vec(Vecu32 vec_primitive, VecAnotherCustomType vec_custom)
        {
            if (vec_primitive.at(0).unwrap() != 10)
            {
                throw new Exception("Returned first u8 from vector doesn't match");
            }
            vec_custom.at(0).unwrap().check_inner_value(99);
            var custom_type = ffi_interface.return_custom_type();
            return custom_type.return_vector_custom();
        }

        public override SharedMutexSomeTrait take_shared_object_and_return(SharedMutexSomeTrait dynamic_shared_type)
        {
            dynamic_shared_type.some_trait_method(99);
            var custom_type = ffi_interface.return_custom_type();
            return custom_type.return_option_with_shared_dynamic_type().unwrap();
        }

        public override u32ResultWithPrimitiveEnum return_ok_result()
        {
            return u32ResultWithPrimitiveEnum.from_ok(10);
        }

        public override u32ResultWithPrimitiveEnum return_err_result()
        {
            return u32ResultWithPrimitiveEnum.from_err(PrimitiveEnum.PrimitiveVariantA);
        }

        public override OptionalRustString return_some_opt()
        {
            return new OptionalRustString(new RustString("Some string from the native application"));
        }

        public override OptionalRustString return_none_opt()
        {
            return new OptionalRustString();
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("C# FFI Test Suite");

            // Assert that Rust traits can be implemented by native languages.
            var a = new TraitImplementation();
            ffi_interface.function_takes_ref_to_trait_object(a);

            // Assert that a global function can return a custom user object.
            CustomType custom_type = ffi_interface.return_custom_type();

            // Assert that an object's method can return a custom user object.
            AnotherCustomType another_custom_type = custom_type.return_static_custom_type();
            another_custom_type.check_inner_value(99);

            // Assert that a function can take a reference to some custom user object.
            custom_type.take_static_custom_type_ref(another_custom_type);

            // Assert that a function can take primitive objects by value.
            custom_type.take_primitive_type(1, 1, 1, 1, (char)1, 1, 1, 1, true);

            // Assert that a function can take and return strings by value.
            if ("String from Rust" != custom_type.take_string_return_another_string(new RustString("String from a foreign language")).to_string())
            {
                throw new Exception("Returned string doesn't match");
            }

            // Assert that a function can return a primitive object by value
            if (custom_type.return_primitive_u8() != 255)
            {
                throw new Exception("Returned u8 value doesn't match");
            }

            // Assert that a function can return an Int64 object by value
            if (custom_type.return_primitive_i64() != -9223372036854775808L)
            {
                throw new Exception("Returned u8 value doesn't match");
            }

            // Assert that a function can return an option object with a custom user object implementing some rust trait
            // wrapped in a reference counted mutex.
            custom_type.return_option_with_shared_dynamic_type().unwrap().some_trait_method(10);

            // Assert that a function can return an option object with a custom user object
            // wrapped in a reference counted mutex.
            custom_type.return_option_with_static_type().unwrap().check_inner_value(99);

            // Assert that a function can return an option object without any object wrapped inside.
            if (custom_type.return_option_with_none().is_some() == true)
            {
                throw new Exception("None option should be present");
            }

            // Assert that a function can take and return vectors by value.
            VecRustString vec_string = custom_type.return_vector_string();
            if (vec_string.at(0).unwrap().to_string() != "First String from Rust")
            {
                throw new Exception("Returned first string from vector doesn't match");
            }
            if (vec_string.at(1).unwrap().to_string() != "Second String from Rust")
            {
                throw new Exception("Returned second string from vector doesn't match");
            }
            Vecu8 vec_primitive = custom_type.return_vector_primitive();
            if (vec_primitive.at(0).unwrap() != 10)
            {
                throw new Exception("Returned first u8 from vector doesn't match");
            }
            if (vec_primitive.at(1).unwrap() != 11)
            {
                throw new Exception("Returned second u8 from vector doesn't match");
            }
            VecAnotherCustomType vec_custom = custom_type.return_vector_custom();
            vec_custom.at(0).unwrap().check_inner_value(10);
            vec_custom.at(1).unwrap().check_inner_value(11);
            vec_string.push(new RustString("String from a foreign language"));
            vec_primitive.push(111);
            vec_custom.push(another_custom_type);
            custom_type.take_vectors(
                vec_string,
                vec_primitive,
                vec_custom
            );

            // Assert that a function can return option object with a custom user object
            // wrapped in a reference counted mutex.
            custom_type.return_option_with_shared_static_type().unwrap().check_inner_value(99);

            // Assert that function can take results and optionals with shared dynamic
            // objects within.
            custom_type.take_option_with_shared_dynamic_type(
                custom_type.return_option_with_shared_dynamic_type()
            );

            // Assert that a global function can take and return strings by value.
            if (ffi_interface.global_function(new RustString("String from a foreign language")).to_string() != "String from Rust")
            {
                throw new Exception("Returned string doesn't match");
            }

            // Assert that a function can take results, options and shared objects by reference.
            custom_type.take_option_and_arc(
                custom_type.return_option_with_static_type(),
                custom_type.return_option_with_shared_static_type().unwrap()
            );

            // Assert taking primitive enums as arguments and returning them from function/method.
            var some_enum = ffi_interface.function_returning_primitive_enum_variant_b_without_args();
            var some_other_enum = ffi_interface.function_taking_primitive_enum_and_returning_primitive_enum(some_enum);
            if (some_other_enum != PrimitiveEnum.PrimitiveVariantB)
            {
                throw new Exception("Returned wrong primitive enum variant from global function");
            }
            ffi_interface.function_taking_primitive_enum(some_other_enum);
            var enum_from_method = custom_type.enum_variant_c(some_other_enum);
            if (enum_from_method != PrimitiveEnum.PrimitiveVariantC)
            {
                throw new Exception("Returned wrong primitive enum variant from method");
            }

            // Data enum tests
            var unit_dce = ffi_interface.function_returning_data_carrying_enum_with_unit_variant();
            if (unit_dce.get_tag() != DataCarryingEnumTag.UnitVariant)
            {
                throw new Exception("Unit variant didn't match");
            }

            var int_dce = ffi_interface.function_returning_data_carrying_enum_with_int32_value_5();
            if (int_dce.get_tag() != DataCarryingEnumTag.Int32Variant
                || int_dce.get_int_32_variant() != 5)
            {
                throw new Exception("int variant didn't match");
            }

            var string_dce = ffi_interface.function_returning_data_carrying_enum_with_string();
            if (string_dce.get_tag() != DataCarryingEnumTag.StringVariant
                || string_dce.get_string_variant().to_string() != "hello")
            {
                throw new Exception("string variant didn't match");
            }

            var unnamed_primitives_dce = ffi_interface.function_returning_data_carrying_enum_with_unnamed_primitive_fields();
            if (unnamed_primitives_dce.get_tag() != DataCarryingEnumTag.UnnamedFieldsVariant
                || unnamed_primitives_dce.get_unnamed_fields_variant().get_0() == 1.1
                || unnamed_primitives_dce.get_unnamed_fields_variant().get_1() != 2
                || !unnamed_primitives_dce.get_unnamed_fields_variant().get_2())
            {
                throw new Exception("Enum with many primitive fields variant didn't match");
            }

            var opaques_dce = ffi_interface.function_returning_data_carrying_enum_with_many_opaque_fields();
            if (opaques_dce.get_tag() != DataCarryingEnumTag.ManyOpaqueFields
                || opaques_dce.get_many_opaque_fields().get_0() != 3
                || opaques_dce.get_many_opaque_fields().get_1().to_string() != "Hello"
                || opaques_dce.get_many_opaque_fields().get_2().return_primitive_u8() != 255
                || opaques_dce.get_many_opaque_fields().get_3().to_string() != "Second Hello")
            {
                throw new Exception("Enum with many opaque fields variant didn't match");
            }

            var opaques_dce_2 = custom_type.method_returning_data_carrying_enum_with_many_opaque_fields();
            if (opaques_dce_2.get_tag() != DataCarryingEnumTag.ManyOpaqueFields
                || opaques_dce_2.get_many_opaque_fields().get_0() != 10
                || opaques_dce_2.get_many_opaque_fields().get_1().to_string() != "Hello"
                || opaques_dce_2.get_many_opaque_fields().get_2().return_primitive_u8() != 255
                || opaques_dce_2.get_many_opaque_fields().get_3().to_string() != "Second Hello")
            {
                throw new Exception("Enum with many opaque fields variant didn't match");
            }
            // TODO WILX-203 exceptions

            var vec_with_opt_string_inside = new VecOptionalRustString();
            vec_with_opt_string_inside.push(new OptionalRustString(new RustString("First inner string")));
            var vec_with_string = new VecRustString();
            vec_with_string.push(new RustString("Second inner string"));
            var vec_with_opt_with_vec_inside = new VecOptionalVecRustString();
            vec_with_opt_with_vec_inside.push(new OptionalVecRustString(vec_with_string));
            if (custom_type.take_and_return_complex_generic_types(
                        new OptionalVecOptionalRustString(vec_with_opt_string_inside),
                        vec_with_opt_with_vec_inside)
                    .at(0)
                    .unwrap()
                    .unwrap()
                    .at(0)
                    .unwrap()
                    .to_string() != "Second inner string")
            {
                throw new Exception("Complex generic type error - received string doesn't match");
            }
        }
    }
}
