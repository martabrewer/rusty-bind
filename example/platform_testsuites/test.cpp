//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <memory>
#include "ffi_cxx.h"
#include <cassert>

class TraitImplementation : public CustomTrait
{
public:
    void empty_method() {}

    u32 take_primitives_return_primitives(u32 a, u32 b) override
    {
        assert(a == 11);
        assert(b == 12);
        return 10;
    }

    String take_string_return_string(String string) override
    {
        assert(string.to_string() == "String from Rust");
        return String("String from a foreign language");
    }

    AnotherCustomType take_custom_obj_return_custom_obj(AnotherCustomType another_custom) override
    {
        another_custom.check_inner_value(99);
        return another_custom;
    }

    RustVec<AnotherCustomType> take_vec_return_vec(RustVec<u32> vec_primitive, RustVec<AnotherCustomType> vec_custom) override
    {
        assert(vec_primitive.at(0).unwrap() == 10);
        vec_custom.at(0).unwrap().check_inner_value(99);
        CustomType custom_type = return_custom_type();
        return RustVec<AnotherCustomType>(custom_type.return_vector_custom());
    }

    SharedMutexSomeTrait take_shared_object_and_return(SharedMutexSomeTrait dynamic_shared_type) override
    {
        dynamic_shared_type.some_trait_method(99);
        CustomType custom_type = return_custom_type();
        return SharedMutexSomeTrait(custom_type.return_option_with_shared_dynamic_type().unwrap());
    }

    u32ResultWithPrimitiveEnum return_ok_result()
    {
        return u32ResultWithPrimitiveEnum::from_ok(10);
    }

    u32ResultWithPrimitiveEnum return_err_result()
    {
        return u32ResultWithPrimitiveEnum::from_err(PrimitiveEnum::PrimitiveVariantA);
    }

    Optional<RustString> return_some_opt()
    {
        return Optional<RustString>(RustString("Some string from the native application"));
    }

    Optional<RustString> return_none_opt()
    {
        return Optional<RustString>();
    }
};

class AsyncHandlerImpl : public AsyncHandler
{
public:
    void callback()
    {
        std::cout << "async callback called" << std::endl;
    }
};

void enum_tests()
{
    CustomType custom_type = return_custom_type();

    // Assert taking primitive enums as arguments and returning them from function/method.
    PrimitiveEnum some_enum = function_returning_primitive_enum_variant_b_without_args();
    PrimitiveEnum some_other_enum = function_taking_primitive_enum_and_returning_primitive_enum(some_enum);
    assert(some_other_enum == PrimitiveEnum::PrimitiveVariantB);

    function_taking_primitive_enum(some_other_enum);
    PrimitiveEnum enum_from_method = custom_type.enum_variant_c(some_other_enum);
    assert(enum_from_method == PrimitiveEnum::PrimitiveVariantC);

    // Tests of data carrying enum

    // unit variant
    DataCarryingEnum unit_dce = function_returning_data_carrying_enum_with_unit_variant();
    assert(unit_dce.get_tag() == DataCarryingEnumTag::UnitVariant);

    // int variant
    DataCarryingEnum int_dce = function_returning_data_carrying_enum_with_int32_value_5();
    assert(int_dce.get_tag() == DataCarryingEnumTag::Int32Variant);
    assert(int_dce.get_int_32_variant() == 5);

    // string variant
    DataCarryingEnum string_dce = function_returning_data_carrying_enum_with_string();
    assert(string_dce.get_tag() == DataCarryingEnumTag::StringVariant);
    assert(string_dce.get_string_variant().to_string() == "hello");

    // single named variant
    DataCarryingEnum single_named_dce = function_returning_data_carrying_enum_with_single_named_field();
    assert(single_named_dce.get_tag() == DataCarryingEnumTag::SingleNamedField);
    assert(single_named_dce.get_single_named_field().to_string() == "Some field");

    // custom type variant
    DataCarryingEnum custom_dce = function_returning_data_carrying_enum_with_custom_type();
    assert(custom_dce.get_tag() == DataCarryingEnumTag::CustomTypeVariant);
    assert(custom_dce.get_custom_type_variant().return_primitive_u8() == 255);

    // many unnamed fields variant
    DataCarryingEnum unnamed_dce = function_returning_data_carrying_enum_with_unnamed_primitive_fields();
    assert(unnamed_dce.get_tag() == DataCarryingEnumTag::UnnamedFieldsVariant);
    assert(unnamed_dce.get_unnamed_fields_variant().get_0() == 1.1f);
    assert(unnamed_dce.get_unnamed_fields_variant().get_1() == 2);
    assert(unnamed_dce.get_unnamed_fields_variant().get_2() == true);

    // many named fields variant
    DataCarryingEnum named_dce = function_returning_data_carrying_enum_with_named_primitive_fields();
    assert(named_dce.get_tag() == DataCarryingEnumTag::NamedTwoFieldsVariant);
    assert(named_dce.get_named_two_fields_variant().get_a() == 1.1f);
    assert(named_dce.get_named_two_fields_variant().get_b() == 2);

    // many opaque types
    DataCarryingEnum multiple_opaque_fields_dce = function_returning_data_carrying_enum_with_many_opaque_fields();
    assert(multiple_opaque_fields_dce.get_tag() == DataCarryingEnumTag::ManyOpaqueFields);
    assert(multiple_opaque_fields_dce.get_many_opaque_fields().get_0() == 3);
    assert(multiple_opaque_fields_dce.get_many_opaque_fields().get_1().to_string() == "Hello");
    assert(multiple_opaque_fields_dce.get_many_opaque_fields().get_2().return_primitive_u8() == 255);
    assert(multiple_opaque_fields_dce.get_many_opaque_fields().get_3().to_string() == "Second Hello");

    // method returning many opaque types
    DataCarryingEnum multiple_opaque_fields_dce_2 = custom_type.method_returning_data_carrying_enum_with_many_opaque_fields();
    assert(multiple_opaque_fields_dce_2.get_many_opaque_fields().get_0() == 10);
    assert(multiple_opaque_fields_dce_2.get_many_opaque_fields().get_1().to_string() == "Hello");
    assert(multiple_opaque_fields_dce_2.get_many_opaque_fields().get_2().return_primitive_u8() == 255);
    assert(multiple_opaque_fields_dce_2.get_many_opaque_fields().get_3().to_string() == "Second Hello");
}

void general_tests()
{
    std::cout << "C++ FFI Test Suite" << std::endl;

    // Assert that Rust traits can be implemented by native languages.
    std::shared_ptr<CustomTrait> custom_trait_obj(new TraitImplementation());
    function_takes_ref_to_trait_object(*custom_trait_obj);

    // Assert that a global function can return a custom user object.
    CustomType custom_type = return_custom_type();

    // Assert that an object's method can return a custom user object.
    AnotherCustomType another_custom_type = custom_type.return_static_custom_type();
    another_custom_type.check_inner_value(99);

    // Assert that a function can take a reference to some custom user object.
    custom_type.take_static_custom_type_ref(another_custom_type);

    // Assert that a function can take primitive objects by value.
    custom_type.take_primitive_type(1, 1, 1, 1, 1, 1, 1, 1, true);

    // Assert that a function can take and return strings by value.
    assert("String from Rust" == custom_type.take_string_return_another_string(String("String from a foreign language")).to_string());

    // Assert that a function can return a primitive object by value
    assert(custom_type.return_primitive_u8() == 255);

    // Assert that a function can return an Int64 object by value
    assert(custom_type.return_primitive_i64() == -9223372036854775808);

    // Assert that a function can return an option object with a custom user object implementing some rust trait
    // wrapped in a reference counted mutex.
    custom_type.return_option_with_shared_dynamic_type().unwrap().some_trait_method(10);

    // Assert that a function can return an option object with a custom user object
    // wrapped in a reference counted mutex.
    custom_type.return_option_with_static_type().unwrap().check_inner_value(99);

    // Assert that a function can return an option object without any object wrapped inside.
    assert(!custom_type.return_option_with_none().is_some());

    // Assert that a function can take and return vectors by value.
    RustVec<String> vec_string = custom_type.return_vector_string();
    assert(vec_string.at(0).unwrap().to_string() == "First String from Rust");
    assert(vec_string.at(1).unwrap().to_string() == "Second String from Rust");

    RustVec<u8> vec_primitive = custom_type.return_vector_primitive();
    assert(vec_primitive.at(0).unwrap() == 10);
    assert(vec_primitive.at(1).unwrap() == 11);

    RustVec<AnotherCustomType> vec_custom = custom_type.return_vector_custom();
    vec_custom.at(0).unwrap().check_inner_value(10);
    vec_custom.at(1).unwrap().check_inner_value(11);
    vec_string.push(String("String from a foreign language"));
    vec_primitive.push(111);
    vec_custom.push(std::move(another_custom_type));
    custom_type.take_vectors(
        vec_string,
        vec_primitive,
        vec_custom);

    // Assert that a function can return option object with a custom user object
    // wrapped in a reference counted mutex.
    custom_type.return_option_with_shared_static_type().unwrap().check_inner_value(99);

    // Assert that function can take optionals with shared dynamic objects inside.
    Optional<SharedMutexSomeTrait> opt = custom_type.return_option_with_shared_dynamic_type();
    custom_type.take_option_with_shared_dynamic_type(opt);

    // Assert that a global function can take and return strings by value.
    assert(global_function(String("String from a foreign language")).to_string() == "String from Rust");

    // Assert that a function can take results, options and shared objects by reference.
    Optional<AnotherCustomType> a = custom_type.return_option_with_static_type();
    SharedMutexAnotherCustomType b = custom_type.return_option_with_shared_static_type().unwrap();
    custom_type.take_option_and_arc(a, b);

    auto async_handler = AsyncHandlerImpl{};
    take_async_handler(async_handler);
}

void exceptions_tests()
{
    // no exceptions
    String str_with_exc = function_raising_exceptions_returns_ok();
    assert(str_with_exc.to_string() == "Ok");

    // exceptions as primitive enum
    try
    {
        String str_with_primitive_exc = function_raising_primitive_exception();
    }
    catch (const PrimitiveEnum_PrimitiveVariantBException &e)
    {
        assert(e.code() == 10);
        assert(e.domain() == Domain::DomainX);
        assert(e.reason().to_string() == "variant B");
        assert(e.what() == "PrimitiveEnum_PrimitiveVariantBException thrown from Rust");
    }

    // thrown exception with primitive inside
    try
    {
        String int_exception = function_raising_exceptions_returns_int_exception();
        assert(false);
    }
    catch (const DataCarryingEnum_Int32VariantException &e)
    {
        assert(e.code() == 2);
        assert(e.domain() == Domain::DomainY);
        assert(e.reason().to_string() == "Int32Variant(420)");
        assert(e.what() == "DataCarryingEnum_Int32VariantException thrown from Rust");
    }

    // thrown exception with opaque type
    try
    {
        String int_exception = function_raising_exceptions_returns_string_exception();
        assert(false);
    }
    catch (const DataCarryingEnum_StringVariantException &e)
    {
        assert(e.code() == 2);
        assert(e.domain() == Domain::DomainY);
        assert(e.reason().to_string() == "StringVariant(\"This is exception from rust\")");
        assert(e.what() == "DataCarryingEnum_StringVariantException thrown from Rust");
    }

    // Ok variant with vector
    RustVec<String> vec_str = function_raising_exceptions_returns_vec_of_strings();
    assert(vec_str.at(0).unwrap().to_string() == "Hello");
    assert(vec_str.at(1).unwrap().to_string() == "from");
    assert(vec_str.at(2).unwrap().to_string() == "Rust");

    // Ok variant with optional
    Optional<String> option_str = function_raising_exceptions_returns_optional_string();
    assert(option_str.unwrap().to_string() == "Hello");

    // thrown exception with ignored field
    try
    {
        Optional<String> int_exception = function_raising_exceptions_returns_ignored_variant_exc();
        assert(false);
    }
    catch (const DataCarryingEnum_IgnoredVariantFieldException &e)
    {
        assert(e.code() == 2);
        assert(e.domain() == Domain::DomainY);
        assert(e.reason().to_string() == "IgnoredVariantField(\"Some string\")");
        assert(e.what() == "DataCarryingEnum_IgnoredVariantFieldException thrown from Rust");
    }

    // void type inside result
    function_raising_exceptions_returns_void();

    // custom type inside result
    CustomType ct = function_raising_exceptions_returns_custom_type();

    // method with ok result
    auto ok_result = ct.method_raising_exceptions_returns_custom_type_ok();

    // method raising exception
    try
    {
        auto err_result = ct.method_raising_exceptions_returns_custom_type_string_err();
        assert(false);
    }
    catch (const DataCarryingEnum_StringVariantException &e)
    {
        assert(e.code() == 2);
        assert(e.domain() == Domain::DomainY);
        assert(e.reason().to_string() == "StringVariant(\"Method exception\")");
        assert(e.what() == "DataCarryingEnum_StringVariantException thrown from Rust");
    }
    auto vec_with_opt_string_inside = RustVec<Optional<String>>();
    vec_with_opt_string_inside.push(Optional<String>(String("First inner string")));
    auto vec_with_string = RustVec<String>();
    vec_with_string.push(String("Second inner string"));
    auto vec_with_opt_with_vec_inside = RustVec<Optional<RustVec<String>>>();
    vec_with_opt_with_vec_inside.push(Optional<RustVec<String>>(vec_with_string));
    assert(ct.take_and_return_complex_generic_types(
                 Optional<RustVec<Optional<String>>>(vec_with_opt_string_inside),
                 vec_with_opt_with_vec_inside)
               .at(0)
               .unwrap()
               .unwrap()
               .at(0)
               .unwrap()
               .to_string() == "Second inner string");
}

int main()
{
    general_tests();
    enum_tests();
    exceptions_tests();
}
