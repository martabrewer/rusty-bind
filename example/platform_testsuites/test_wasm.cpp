//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "ffi_cxx.h"
#include <emscripten.h>

int main(void)
{
    emscripten_run_script(R"(
    var TraitImplementation = Module.CustomTrait.extend("CustomTrait", {
            empty_method: function() {},

            take_primitives_return_primitives: function(a, b) {
                if(a != 11) {
                    throw new Exception("Primitive type value doesn't match in exposed Rust trait (a).");
                }
                if(b != 12) {
                    throw new Exception("Primitive type value doesn't match in exposed Rust trait (b).");
                }
                return 10;
            },

            take_string_return_string: function (string_) {
                if(string_.to_string() != "String from Rust") {
                    throw new Exception("Passed string doesn't match");
                }
                return new Module.String("String from a foreign language");
            },

            take_custom_obj_return_custom_obj: function(another_custom) {
                another_custom.check_inner_value(99);
                var custom_type = Module.return_custom_type();
                return custom_type.return_static_custom_type();
            },

            take_vec_return_vec: function(vec_primitive, vec_custom) {
                if(vec_primitive.at(0).unwrap() != 10) {
                    throw new Exception("Returned first u8 from vector doesn't match");
                }
                vec_custom.at(0).unwrap().check_inner_value(99);
                var custom_type = Module.return_custom_type();
                return custom_type.return_vector_custom();
            },

            take_shared_object_and_return: function(dynamic_shared_type) {
                dynamic_shared_type.some_trait_method(99);
                var custom_type = Module.return_custom_type();
                return custom_type.return_option_with_shared_dynamic_type().unwrap();
            },

            return_ok_result: function() {
                return Module.u32ResultWithPrimitiveEnum.from_ok(10);
            },

            return_err_result: function() {
                return Module.u32ResultWithPrimitiveEnum.from_err(Module.PrimitiveEnum.PrimitiveVariantA);
            },

            return_some_opt: function() {
                return new Module.OptionalRustString(new Module.String("Some string from the native application"));
            },

            return_none_opt: function() {
                return new Module.OptionalRustString();
            }
        });

        // Assert that Rust traits can be implemented by native languages.
        var a = new TraitImplementation;
        Module.function_takes_ref_to_trait_object(a);

        // Assert that a global function can return a custom user object.
        let custom_type = Module.return_custom_type();

        // Assert that an object's method can return a custom user object.
        let another_custom_type = custom_type.return_static_custom_type();
        another_custom_type.check_inner_value(99);

        // Assert that a function can take a reference to some custom user object.
        custom_type.take_static_custom_type_ref(another_custom_type);

        // NOTE: Emscripten (WebAssembly) does not support 64-bit primitives
        // Assert that a function can take primitive objects by value.
        // custom_type.take_primitive_type(1,1,1,1,1,1,1,1,true);

        // Assert that a function can take and return strings by value.
        if(!custom_type.take_string_return_another_string(new Module.String("String from a foreign language")).to_string() === "String from Rust") {
            throw new Error("Returned string doesn't match");
        }

        // Assert that a function can return a primitive object by value
        if(custom_type.return_primitive_u8() != 255) {
            throw new Error("Returned u8 value doesn't match");
        }

        // NOTE: Emscripten (WebAssembly) does not support 64-bit primitives
        // Assert that a function can return an Int64 object by value
        // if(custom_type.return_primitive_i64() != -9223372036854775808) {
        //     throw new Error("Returned u8 value doesn't match");
        // }

        // Assert that a function can return an option object with a custom user object implementing some rust trait
        // wrapped in a reference counted mutex.
        custom_type.return_option_with_shared_dynamic_type().unwrap().some_trait_method(10);

        // Assert that a function can return an option object with a custom user object
        // wrapped in a reference counted mutex.
        custom_type.return_option_with_static_type().unwrap().check_inner_value(99);

        // Assert that a function can return an option object without any object wrapped inside.
        if(custom_type.return_option_with_none().is_some() == true) {
            throw new Error("None option should be present");
        }

        // Assert that a function can take and return vectors by value.
        let vec_string = custom_type.return_vector_string();
        if(!vec_string.at(0).unwrap().to_string() === "First String from Rust") {
            throw new Error("Returned first string from vector doesn't match");
        }
        if(!vec_string.at(1).unwrap().to_string() === "Second String from Rust") {
            throw new Error("Returned second string from vector doesn't match");
        }
        let vec_primitive = custom_type.return_vector_primitive();
        if(vec_primitive.at(0).unwrap() != 10) {
            throw new Error("Returned first u8 from vector doesn't match");
        }
        if(vec_primitive.at(1).unwrap() != 11) {
            throw new Error("Returned second u8 from vector doesn't match");
        }
        let vec_custom = custom_type.return_vector_custom();
        vec_custom.at(0).unwrap().check_inner_value(10);
        vec_custom.at(1).unwrap().check_inner_value(11);
        vec_string.push(new Module.String("String from a foreign language"));
        vec_primitive.push(111);
        vec_custom.push(another_custom_type);
        custom_type.take_vectors(
            vec_string,
            vec_primitive,
            vec_custom
        );
        another_custom_type.delete()
        vec_string.delete()
        vec_primitive.delete()
        vec_custom.delete()

        // Assert that a function can return option object with a custom user object
        // wrapped in a reference counted mutex.
        custom_type.return_option_with_shared_static_type().unwrap().check_inner_value(99);

        // Assert that function can take optionals with shared dynamic
        // objects within.
        custom_type.take_option_with_shared_dynamic_type(
            custom_type.return_option_with_shared_dynamic_type()
        );

        // Assert that a global function can take and return strings by value.
        if(!("String from Rust" === Module.global_function(new Module.String("String from a foreign language")).to_string())) {
            throw new Error("Returned string doesn't match");
        }

        // Assert that a function can take results, options and shared objects by reference.
        custom_type.take_option_and_arc(
            custom_type.return_option_with_static_type(),
            custom_type.return_option_with_shared_static_type().unwrap()
        );

        // Assert taking primitive enums as arguments and returning them from function/method.
        var some_enum = Module.function_returning_primitive_enum_variant_b_without_args();
        var some_other_enum = Module.function_taking_primitive_enum_and_returning_primitive_enum(some_enum);
        if (some_other_enum != Module.PrimitiveEnum.PrimitiveVariantB)
        {
            throw new Error("Returned wrong primitive enum variant from global function");
        }
        Module.function_taking_primitive_enum(some_other_enum);
        var enum_from_method = custom_type.enum_variant_c(some_other_enum);
        if (enum_from_method != Module.PrimitiveEnum.PrimitiveVariantC)
        {
            throw new Error("Returned wrong primitive enum variant from method");
        }

        // Data enum tests
        var unit_dce = Module.function_returning_data_carrying_enum_with_unit_variant();
        if (unit_dce.get_tag() != Module.DataCarryingEnumTag.UnitVariant){
            throw new Error("Unit variant didn't match");
        }

        var int_dce = Module.function_returning_data_carrying_enum_with_int32_value_5();
        if (int_dce.get_tag() != Module.DataCarryingEnumTag.Int32Variant
            || int_dce.get_int_32_variant() != 5) {
            throw new Error("int variant didn't match");
        }

        var string_dce = Module.function_returning_data_carrying_enum_with_string();
        if (string_dce.get_tag() != Module.DataCarryingEnumTag.StringVariant
            || string_dce.get_string_variant().to_string() !== "hello") {
            throw new Error("string variant didn't match");
        }

        var unnamed_primitives_dce = Module.function_returning_data_carrying_enum_with_unnamed_primitive_fields();
        if (unnamed_primitives_dce.get_tag() != Module.DataCarryingEnumTag.UnnamedFieldsVariant
            || Math.abs(unnamed_primitives_dce.get_unnamed_fields_variant().get_0() - 1.1) > 0.00001
            || unnamed_primitives_dce.get_unnamed_fields_variant().get_1() != 2
            || !unnamed_primitives_dce.get_unnamed_fields_variant().get_2() ) {
            throw new Error("Enum with many primitive fields variant didn't match");
        }

        var opaques_dce = Module.function_returning_data_carrying_enum_with_many_opaque_fields();
        if (opaques_dce.get_tag() != Module.DataCarryingEnumTag.ManyOpaqueFields
            || opaques_dce.get_many_opaque_fields().get_0() != 3
            || opaques_dce.get_many_opaque_fields().get_1().to_string() !== "Hello"
            || opaques_dce.get_many_opaque_fields().get_2().return_primitive_u8() != 255
            || opaques_dce.get_many_opaque_fields().get_3().to_string() !== "Second Hello" ) {
            throw new Error("Enum with many opaque fields variant didn't match");
        }

        var opaques_dce = custom_type.method_returning_data_carrying_enum_with_many_opaque_fields();
        if (opaques_dce.get_tag() != Module.DataCarryingEnumTag.ManyOpaqueFields
            || opaques_dce.get_many_opaque_fields().get_0() != 10
            || opaques_dce.get_many_opaque_fields().get_1().to_string() !== "Hello"
            || opaques_dce.get_many_opaque_fields().get_2().return_primitive_u8() != 255
            || opaques_dce.get_many_opaque_fields().get_3().to_string() !== "Second Hello" ) {
            throw new Error("Enum with many opaque fields variant didn't match");
        }

        var vec_with_opt_string_inside = new Module.VecOptionalRustString()
        vec_with_opt_string_inside.push(new Module.OptionalRustString(new Module.String("First inner string")))
        var vec_with_string = new Module.VecRustString()
        vec_with_string.push(new Module.String("Second inner string"))
        var vec_with_opt_with_vec_inside = new Module.VecOptionalVecRustString()
        vec_with_opt_with_vec_inside.push(new Module.OptionalVecRustString(vec_with_string))
        if (custom_type.take_and_return_complex_generic_types(
                    new Module.OptionalVecOptionalRustString(vec_with_opt_string_inside),
                    vec_with_opt_with_vec_inside)
                .at(0)
                .unwrap()
                .unwrap()
                .at(0)
                .unwrap()
                .to_string() != "Second inner string") {
            throw new Error("Complex generic type error - received string doesn't match");
        }

        custom_type.delete()

        // no exceptions
        var str_with_exc = Module.function_raising_exceptions_returns_ok();
        assert(str_with_exc.to_string() == "Ok");

        // exceptions as primitive enum
        try
        {
            var str_with_primitive_exc = Module.function_raising_primitive_exception();
            assert(false);
        }
        catch (e)
        {
            var exception = new Module.RustException(e);
            assert(exception.exception_class() == Module.ExceptionClass.PrimitiveEnum_PrimitiveVariantBException);
            assert(exception.code() == 10);
            assert(exception.domain() == Module.Domain.DomainX);
            assert(exception.reason().to_string() == "variant B");
            assert(exception.what() == "PrimitiveEnum_PrimitiveVariantBException thrown from Rust");
        }

        // thrown exception with primitive inside
        try
        {
            var int_exception = Module.function_raising_exceptions_returns_int_exception();
            assert(false);
        }
        catch (e)
        {
            var exception = new Module.RustException(e);
            assert(exception.exception_class() == Module.ExceptionClass.DataCarryingEnum_Int32VariantException);
            assert(exception.code() == 2);
            assert(exception.domain() == Module.Domain.DomainY);
            assert(exception.reason().to_string() == 'Int32Variant(420)');
            assert(exception.what() == "DataCarryingEnum_Int32VariantException thrown from Rust");
        }

        // thrown exception with opaque type
        try
        {
            var int_exception = Module.function_raising_exceptions_returns_string_exception();
            assert(false);
        }
        catch (e)
        {
            var exception = new Module.RustException(e);
            assert(exception.exception_class() == Module.ExceptionClass.DataCarryingEnum_StringVariantException);
            assert(exception.code() == 2);
            assert(exception.domain() == Module.Domain.DomainY);
            assert(exception.reason().to_string() == 'StringVariant(\"This is exception from rust\")');
            assert(exception.what() == "DataCarryingEnum_StringVariantException thrown from Rust");
        }

        // Ok variant with vector
        var vec_str = Module.function_raising_exceptions_returns_vec_of_strings();
        assert(vec_str.at(0).unwrap().to_string() == "Hello");
        assert(vec_str.at(1).unwrap().to_string() == "from");
        assert(vec_str.at(2).unwrap().to_string() == "Rust");

        // Ok variant with optional
        var option_str = Module.function_raising_exceptions_returns_optional_string();
        assert(option_str.unwrap().to_string() == "Hello");

        // thrown exception with ignored field
        try
        {
            var int_exception = Module.function_raising_exceptions_returns_ignored_variant_exc();
            assert(false);
        }
        catch (e)
        {
            var exception = new Module.RustException(e);
            assert(exception.exception_class() == Module.ExceptionClass.DataCarryingEnum_IgnoredVariantFieldException);
            assert(exception.code() == 2);
            assert(exception.domain() == Module.Domain.DomainY);
            assert(exception.reason().to_string() == 'IgnoredVariantField(\"Some string\")');
            assert(exception.what() == "DataCarryingEnum_IgnoredVariantFieldException thrown from Rust");
        }

        // void type inside result
        Module.function_raising_exceptions_returns_void();

        // custom type inside result
        var ct = Module.function_raising_exceptions_returns_custom_type();

        // ok result
        var ok_result = ct.method_raising_exceptions_returns_custom_type_ok();
        try
        {
            var err_result = ct.method_raising_exceptions_returns_custom_type_string_err();
            assert(false);
        }
        catch (e)
        {
            var exception = new Module.RustException(e);
            assert(exception.exception_class() == Module.ExceptionClass.DataCarryingEnum_StringVariantException);
            assert(exception.code() == 2);
            assert(exception.domain() == Module.Domain.DomainY);
            assert(exception.reason().to_string() == 'StringVariant(\"Method exception\")');
            assert(exception.what() == 'DataCarryingEnum_StringVariantException thrown from Rust');
        }
        ct.delete()

        // test vectors initialization
        var str_vec = new Module.VecRustString();
        str_vec.push(new Module.String("hello"));
        assert(str_vec.at(0).unwrap().to_string() === "hello");
        var u8_vec = new Module.Vecu8();
    )");

    return 0;
}
