use cfg_expr::{Expression, Predicate};
use quote::quote;
use syn::Attribute;

use crate::{binding_module::CargoFeature, BuildContext};

pub(crate) fn check_cfg_attr(attr: &Attribute, context: &BuildContext) -> bool {
    let tokens = &attr.tokens;
    let cfg = quote! { cfg #tokens }.to_string().replace(' ', "");
    let required_cfg = Expression::parse(&cfg).expect("Invalid cfg attribute");

    required_cfg.eval(|pred| match pred {
        Predicate::Target(tp) => tp.matches(context.current_target_info),
        Predicate::Feature(feature) => context
            .features
            .iter()
            .any(|val| *val == CargoFeature::new(feature)),
        Predicate::TargetFeature(target_feature) => context
            .target_features
            .iter()
            .any(|val| val == target_feature),
        _ => panic!("Unsupported cfg value"),
    })
}

#[cfg(test)]
pub(crate) mod helpers {
    use crate::{enum_helpers::get_enums_from_module, BuildContext};
    use cfg_expr::targets::get_builtin_target_by_triple;
    use syn::ItemEnum;

    pub fn get_context() -> BuildContext {
        BuildContext {
            current_target_info: get_builtin_target_by_triple("x86_64-apple-darwin").unwrap(),
            features: vec![],
            target_features: vec![],
        }
    }

    pub fn get_enum_item() -> ItemEnum {
        let rust_code = "
mod ffi {
    enum En1 {
        V1,
        V2,
    }
}
        ";
        let context = get_context();
        let module = syn::parse_str(rust_code).unwrap();
        get_enums_from_module(&module, &context).unwrap().remove(0)
    }
}
