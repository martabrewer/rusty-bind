//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub use crate::binding_types::*;
use crate::{
    binding_module::EXTERN_EXCEPTION_TRAIT_MODULE_NAME, cpp::exception_class_name,
    enum_helpers::is_primitive_enum, ordered_hash_set::OrderedHashSet, utils::check_cfg_attr,
    BuildContext,
};
use anyhow::{anyhow, Context};
use proc_macro2::Ident;
use quote::ToTokens;
use std::collections::{HashMap, HashSet};
use syn::{
    parse_quote, punctuated::Punctuated, token::Add, FnArg, ForeignItem, ForeignItemFn,
    ForeignItemType, GenericArgument, ItemEnum, ItemForeignMod, Pat, PatIdent, PathArguments,
    PathSegment, Receiver, ReturnType, TraitBound, Type, TypeParamBound, TypeReference,
    TypeTraitObject,
};

/// The structure keeps the Rust types wrappers,
/// methods of the structures and global functions
/// signatures. `user_custom_types` is a subset
/// of `rust_types_wrappers`, since the latter collection
/// keeps all the wrappers that will be handled, and the
/// first one keeps only a user defined types wrappers
/// along with their methods.
///
#[derive(Default)]
pub struct ExternModuleTranslator {
    pub shared_enums: HashSet<ItemEnum>,
    pub exception_names: HashSet<String>,
    pub rust_types_wrappers: OrderedHashSet<WrapperType>,
    pub user_custom_types: HashMap<WrapperType, Vec<Function>>,
    pub user_traits: HashMap<WrapperType, Vec<Function>>,
    pub global_functions: Vec<Function>,
    pub exception_trait_methods: HashSet<Function>,
}

impl ExternModuleTranslator {
    pub fn new(shared_enums: HashSet<ItemEnum>) -> Self {
        let mut rust_types_wrappers = OrderedHashSet::default();
        shared_enums
            .iter()
            .for_each(|enum_item| rust_types_wrappers.insert(parse_enum_wrapper(enum_item)));
        Self {
            shared_enums,
            rust_types_wrappers,
            ..Default::default()
        }
    }

    /// Takes extern "Rust" module and translates its functions.
    /// It also stores information about the functions signatures
    /// for a further processing.
    ///
    pub fn parse_items_in_extern_rust_module(
        &mut self,
        extern_module: &ItemForeignMod,
        context: &BuildContext,
    ) -> anyhow::Result<()> {
        extern_module
            .items
            .iter()
            .filter(|extern_item| match extern_item {
                ForeignItem::Fn(function) => function
                    .attrs
                    .iter()
                    .map(|attr| check_cfg_attr(attr, context))
                    .all(|el| el),
                ForeignItem::Type(type_) => type_
                    .attrs
                    .iter()
                    .map(|attr| check_cfg_attr(attr, context))
                    .all(|el| el),
                _ => true,
            })
            .try_for_each(|extern_item| match extern_item {
                ForeignItem::Fn(function) => self.register_new_rust_function(function.clone()),
                ForeignItem::Type(ForeignItemType { ident, .. }) => {
                    self.register_custom_type(ident.clone());
                    Ok(())
                }
                _ => Ok(()),
            })
    }

    /// Takes extern "Traits" module and translates its functions.
    /// It also stores information about the functions signatures
    /// for a further processing.
    ///
    pub fn translate_trait_external_modules(
        &mut self,
        extern_module: &ItemForeignMod,
        context: &BuildContext,
    ) -> anyhow::Result<()> {
        extern_module
            .items
            .iter()
            .filter(|extern_item| match extern_item {
                ForeignItem::Fn(function) => function
                    .attrs
                    .iter()
                    .map(|attr| check_cfg_attr(attr, context))
                    .all(|el| el),
                _ => true,
            })
            .try_for_each(|extern_item| match extern_item {
                ForeignItem::Fn(original_function) => {
                    let (associated_structure, function) =
                        self.translate_function(original_function.clone())?;
                    if let Some(custom_trait) = associated_structure {
                        self.user_traits
                            .entry(custom_trait)
                            .or_default()
                            .push(function);
                        Ok(())
                    } else {
                        Err(anyhow!(
                            "Global functions are not supported in extern \"Traits\"."
                        ))
                    }
                }
                _ => Ok(()),
            })
    }

    /// Takes extern "ExceptionTrait" module and translates its methods.
    /// It also stores information about the functions signatures
    /// for a further processing.
    ///
    pub fn translate_exception_trait_external_module(
        &mut self,
        extern_module: &ItemForeignMod,
        context: &BuildContext,
    ) -> anyhow::Result<()> {
        extern_module
            .items
            .iter()
            .filter(|extern_item| match extern_item {
                ForeignItem::Fn(function) => function
                    .attrs
                    .iter()
                    .map(|attr| check_cfg_attr(attr, context))
                    .all(|el| el),
                _ => true,
            })
            .try_for_each(|extern_item| match extern_item {
                ForeignItem::Fn(function) => {
                    let method = self.translate_exception_trait_function(function.clone())?;
                    self.exception_trait_methods.insert(method);
                    Ok(())
                }
                _ => Err(anyhow!(
                    "Only functions are acceptable in `ExceptionsTrait`"
                )),
            })
    }

    /// Method stores a wrapper to a custom user's type.
    /// Can be used while parsing `type SomeUserType;`
    /// to register a new type `RustUserType` in
    /// the collection of intermediate-form wrappers.
    ///
    fn register_custom_type(&mut self, original_type_name: Ident) -> WrapperType {
        let new_wrapper_type = WrapperType {
            original_type_name: parse_quote!( #original_type_name ),
            wrapper_name: original_type_name.to_string(),
            rust_type: RustWrapperType::Custom,
            is_ref: false,
            is_mut: false,
        };
        self.rust_types_wrappers.insert(new_wrapper_type.clone());
        self.user_custom_types
            .insert(new_wrapper_type.clone(), vec![]);
        new_wrapper_type
    }

    /// Example:
    /// `fn foo(self: &SomeType) -> Result<AnotherType>` ==>
    ///     `fn foo(self: &SomeType) -> ResultAnotherType`.
    ///
    /// The method takes the Rust function signature and
    /// registers every type as a `WrapperType` as well as
    /// translates it into inner function form `Function`.
    /// If the type has a `self` argument it returns its
    /// WrapperType in the result.
    ///
    /// Example:
    /// ```rust
    /// use syn::{ForeignItemFn, parse_quote};
    /// use rusty_bind_parser::extern_module_translator::*;
    ///
    ///
    /// let mut local_module_translator = ExternModuleTranslator::default();
    /// let function: ForeignItemFn = parse_quote! { fn foo(self: &SomeType, a: u32) -> Option<u32>; };
    /// if let (Some(associated_type), Function {
    ///     arguments,
    ///     return_type: Some(return_type),
    ///     name,
    /// }) = local_module_translator.translate_function(function).unwrap() {
    ///     assert!(name == "foo");
    ///     assert!(arguments.len() == 2);
    ///     assert!(arguments[0] == Arg {arg_name: "self".to_owned(), typ: WrapperType {
    ///         original_type_name: parse_quote! { SomeType },
    ///         wrapper_name: "SomeType".to_owned(),
    ///         rust_type: RustWrapperType::Custom,
    ///         is_ref: true,
    ///         is_mut: false,
    ///     }});
    ///     assert!(arguments[1] == Arg {arg_name: "a".to_owned(), typ: WrapperType {
    ///         original_type_name: parse_quote! { u32 },
    ///         wrapper_name: "u32".to_owned(),
    ///         rust_type: RustWrapperType::Primitive,
    ///         is_ref: false,
    ///         is_mut: false,
    ///     }});
    ///     assert!(associated_type == WrapperType {
    ///         original_type_name: parse_quote! { SomeType },
    ///         wrapper_name: "SomeType".to_owned(),
    ///         rust_type: RustWrapperType::Custom  ,
    ///         is_ref: true,
    ///         is_mut: false,
    ///     });
    ///     println!("{return_type:?}");
    ///     assert!(return_type == WrapperType {
    ///         original_type_name: parse_quote! { Option<u32> },
    ///         wrapper_name: "Optionalu32".to_owned(),
    ///         rust_type: RustWrapperType::Option(
    ///             WrapperType {
    ///                 original_type_name: parse_quote! { u32 },
    ///                 wrapper_name: "u32".to_owned(),
    ///                 rust_type: RustWrapperType::Primitive,
    ///                 is_ref: false,
    ///                 is_mut: false,
    ///             }.boxed()
    ///         ),
    ///         is_ref: false,
    ///         is_mut: false,
    ///     });
    /// } else {
    ///     panic!("Translated function doesn't match.");
    /// }
    /// ```
    ///
    pub fn translate_function(
        &mut self,
        mut function: ForeignItemFn,
    ) -> anyhow::Result<(Option<WrapperType>, Function)> {
        let mut arguments = vec![];
        let mut associated_structure = None;

        function
            .sig
            .inputs
            .iter_mut()
            .try_for_each(|argument| match argument {
                FnArg::Typed(argument) => {
                    let new_wrapper_type = self
                        .parse_and_register_rust_type(argument.ty.as_mut())
                        .with_context(|| {
                            format!(
                                "Unsupported type in function signature: {}",
                                function.sig.ident
                            )
                        })?;
                    // TODO WILX-184
                    if new_wrapper_type.rust_type == RustWrapperType::DataEnum {
                        return Err(anyhow!(
                            "Data carrying enums as functions arguments are not supported yet!"
                        ));
                    };
                    if let Pat::Ident(PatIdent { ident, .. }) = argument.pat.as_ref() {
                        arguments.push(Arg {
                            arg_name: ident.to_string(),
                            typ: new_wrapper_type.clone(),
                        });
                        if *ident == "self" {
                            associated_structure = Some(new_wrapper_type);
                        }
                    }
                    Ok(())
                }
                _ => Err(anyhow!(
                    "Only typed arguments are supported (no bare `self`) in function: `{}`",
                    function.sig.ident
                )),
            })?;
        let return_type = self.translate_return_type(&mut function)?;
        let function_name = function.sig.ident.clone();
        Ok((
            associated_structure,
            Function {
                arguments,
                return_type,
                name: function_name.to_string(),
            },
        ))
    }

    fn translate_exception_trait_function(
        &mut self,
        mut function: ForeignItemFn,
    ) -> anyhow::Result<Function> {
        if function.sig.inputs.len() != 1 {
            Err(anyhow!(
                "Methods of ExceptionTrait must have only one argument: &self"
            ))
        } else if let FnArg::Receiver(Receiver {
            reference: Some((_, None)),
            ..
        }) = function.sig.inputs.first().unwrap()
        {
            let return_type = self.translate_return_type(&mut function)?;
            let associated_structure = WrapperType {
                original_type_name: parse_quote! {ExceptionTrait},
                wrapper_name: EXTERN_EXCEPTION_TRAIT_MODULE_NAME.to_string(),
                rust_type: RustWrapperType::ExceptionTrait,
                is_ref: true,
                is_mut: false,
            };
            self.rust_types_wrappers
                .insert(associated_structure.clone());
            Ok(Function {
                arguments: vec![Arg {
                    arg_name: "self".to_owned(),
                    typ: associated_structure,
                }],
                return_type,
                name: function.sig.ident.to_string(),
            })
        } else {
            Err(anyhow!(
                "Methods of ExceptionTrait must have argument &self"
            ))
        }
    }

    fn translate_return_type(
        &mut self,
        function: &mut ForeignItemFn,
    ) -> anyhow::Result<Option<WrapperType>> {
        Ok(if let ReturnType::Type(_, typ) = &mut function.sig.output {
            let new_wrapper_type = self
                .parse_and_register_rust_type(typ.as_mut())
                .with_context(|| {
                    format!(
                        "Unsupported return type in function `{}`",
                        function.sig.ident.clone(),
                    )
                })?;
            Some(new_wrapper_type)
        } else {
            None
        })
    }

    /// Method registers the function in the local state of ExternModuleTranslator:
    ///  * function signature
    ///  * every type used in arguments and return statement
    ///
    fn register_new_rust_function(&mut self, function: ForeignItemFn) -> anyhow::Result<()> {
        let (associated_structure, result_function) = self.translate_function(function)?;
        if let Some(custom_type) = associated_structure {
            self.user_custom_types
                .entry(custom_type)
                .or_default()
                .push(result_function);
        } else {
            self.global_functions.push(result_function)
        }
        Ok(())
    }

    /// Translates <T> into T.
    /// Example:
    ///  * <Result<u8>> --> Result<u8>
    ///
    fn get_inner_generic_type(path_segment: &PathSegment) -> anyhow::Result<&Type> {
        match &path_segment.arguments {
            PathArguments::AngleBracketed(args) => args
                .args
                .first()
                .with_context(|| format!("Unsupported generic definition: `{path_segment:?}`"))
                .and_then(|generic_argument| match generic_argument {
                    GenericArgument::Type(typ) => Ok(typ),
                    _ => Err(anyhow!(
                        "Unsupported generic definition: `{path_segment:?}`"
                    )),
                }),
            _ => Err(anyhow!(
                "Expected inner generic type specification: `{path_segment:?}`"
            )),
        }
    }

    /// Extracts inner generic types.
    /// Example:
    ///  * Result<u8, i32> --> vec[u8, i32]
    ///
    fn get_inner_generic_types(path_segment: &PathSegment) -> anyhow::Result<Vec<&Type>> {
        match &path_segment.arguments {
            PathArguments::AngleBracketed(args) => args
                .args
                .iter()
                .map(|generic_argument| match generic_argument {
                    GenericArgument::Type(typ) => Ok(typ),
                    _ => Err(anyhow!("Unsupported generic type")),
                })
                .collect(),
            _ => panic!("get_inner_generic_types called in bad context"),
        }
    }

    /// Checks if the type is `&dyn Trait` and returns a proper WrapperType in
    /// such a case.
    ///
    fn translate_trait_wrapper_type(
        &mut self,
        bounds: &Punctuated<TypeParamBound, Add>,
        is_mut: bool,
    ) -> anyhow::Result<WrapperType> {
        bounds
            .first()
            .and_then(|trait_token| {
                if let TypeParamBound::Trait(TraitBound { path, .. }) = trait_token {
                    path.segments.first().map(|path_segment| {
                        let new_wrapper_type = WrapperType {
                            original_type_name: parse_quote!( #path ),
                            wrapper_name: path_segment.ident.to_string(),
                            rust_type: RustWrapperType::Trait,
                            is_ref: true,
                            is_mut,
                        };
                        self.rust_types_wrappers.insert(new_wrapper_type.clone());
                        Ok(new_wrapper_type)
                    })
                } else {
                    Some(Err(anyhow!(
                        "Lifetimes in TraitObject `{bounds:?}` are not supported"
                    )))
                }
            })
            .unwrap_or_else(|| {
                Err(anyhow!(
                    "Problem occurred during parsing the &dyn type: Empty type `{bounds:?}`"
                ))
            })
    }

    fn parse_rust_exceptions_type(&mut self, typ: &Type) -> anyhow::Result<WrapperType> {
        match typ {
            Type::Path(path) => path
                .path
                .segments
                .first()
                .context("Invalid enum indicating possible exceptions")
                .and_then(|path_segment| {
                    let enum_ident = &path_segment.ident;
                    let enum_item = self
                        .get_enum(enum_ident.to_string().as_str())
                        .context("Result's 2nd argument must be an enum's ident.")?;
                    let exc_variants = enum_item
                        .variants
                        .iter()
                        .map(|v| v.ident.clone())
                        .collect::<Vec<_>>();

                    let exception_wrapper = WrapperType {
                        original_type_name: typ.clone(),
                        wrapper_name: enum_ident.to_string(),
                        rust_type: if is_primitive_enum(enum_item) {
                            RustWrapperType::Exceptions(Exceptions::Primitive(exc_variants.clone()))
                        } else {
                            RustWrapperType::Exceptions(Exceptions::NonPrimitive(
                                exc_variants.clone(),
                            ))
                        },
                        is_ref: false,
                        is_mut: false,
                    };

                    self.exception_names.extend(
                        exc_variants
                            .iter()
                            .map(|variant_ident| exception_class_name(enum_ident, variant_ident)),
                    );
                    self.rust_types_wrappers.insert(exception_wrapper.clone());

                    Ok(exception_wrapper)
                }),
            _ => Err(anyhow!("Could not parse exception type")),
        }
    }

    /// Method takes a type (for instance `Arc<Mutex<dyn SomeCustomType>>`)
    /// and creates one or more intermediate-form wrappers which are remembered in ExternModuleTranslator.
    /// Example:
    /// The following wrappers are created out of Result<Arc<Mutex<dyn SomeCustomType>>>:
    ///  * ResultSharedMutexSomeCustomType - Result<Arc<Mutex<dyn SomeCustomType>>>
    ///  * SharedMutexSomeCustomType - Arc<Mutex<dyn SomeCustomType>>
    ///
    /// Note: This method is called recursively for Results, Options and Vectors,
    ///       but for `Arc<T>` and `Rc<T>` the `T` is transparent for the parser.
    ///
    fn parse_and_register_rust_type(&mut self, typ: &Type) -> anyhow::Result<WrapperType> {
        match typ {
            Type::Path(path) => path
                .path
                .segments
                .first()
                .with_context(|| format!("Unsupported type `{typ:?}`"))
                .and_then(|path_segment| {
                    let ident_str = path_segment.ident.to_string();
                    let new_wrapper_type = if let Some(enum_item) = self.get_enum(&ident_str) {
                        parse_enum_wrapper(enum_item)
                    } else {
                        match ident_str.as_ref() {
                            "Result" => self.parse_result_wrapper(typ.clone(), path_segment)?,
                            "Option" => self.parse_option_wrapper(typ.clone(), path_segment)?,
                            "Vec" => self.parse_vec_wrapper(typ.clone(), path_segment)?,
                            "Arc" => parse_arc_wrapper(typ.clone(), path_segment)?,
                            "String" => parse_string_wrapper(typ.clone()),
                            primitive @ ("u8" | "u16" | "u32" | "u64" | "u128" | "i8" | "i16"
                            | "i32" | "i64" | "i128" | "f8" | "f16" | "f32"
                            | "f64" | "f128" | "usize" | "bool") => {
                                parse_primitive_wrapper(typ.clone(), primitive)
                            }
                            _ => parse_custom_wrapper(typ.clone(), path_segment),
                        }
                    };

                    // The following additional `Option<T>` type is used by
                    // the `Vec<T>::get(..) -> Option<T>;` function:
                    if let RustWrapperType::Vector(inner_type) = &new_wrapper_type.rust_type {
                        let inner_type_original = &inner_type.original_type_name;
                        let option_wrapper = WrapperType {
                            original_type_name: parse_quote! { Option<#inner_type_original> },
                            wrapper_name: format!("Optional{}", &inner_type.wrapper_name),
                            rust_type: RustWrapperType::Option(inner_type.clone().boxed()),
                            is_ref: false,
                            is_mut: false,
                        };
                        self.rust_types_wrappers.insert(option_wrapper);
                    }

                    self.rust_types_wrappers.insert(new_wrapper_type.clone());
                    Ok(new_wrapper_type)
                }),
            Type::Reference(TypeReference {
                elem, mutability, ..
            }) => {
                if let Type::TraitObject(TypeTraitObject { bounds, .. }) = elem.as_ref() {
                    self.translate_trait_wrapper_type(bounds, mutability.is_some())
                } else {
                    let wrapper = self.parse_and_register_rust_type(elem.as_ref());
                    wrapper.map(|wrapper| WrapperType {
                        is_ref: true,
                        is_mut: mutability.is_some(),
                        ..wrapper
                    })
                }
            }
            _ => Err(anyhow!("Unsupported type `{typ:?}`")),
        }
    }

    fn parse_result_wrapper(
        &mut self,
        original_type: Type,
        path_segment: &PathSegment,
    ) -> anyhow::Result<WrapperType> {
        ExternModuleTranslator::get_inner_generic_types(path_segment).and_then(
            |mut generic_types| {
                let ok_type = self.parse_and_register_rust_type(
                    generic_types
                        .get_mut(0)
                        .context("Result should have 2 generic types")?,
                )?;
                let exceptions_type = self.parse_rust_exceptions_type(
                    generic_types
                        .get_mut(1)
                        .context("Result should have 2 generic types")?,
                )?;
                Ok(WrapperType {
                    original_type_name: original_type,
                    wrapper_name: format!(
                        "{}ResultWith{}",
                        &ok_type.wrapper_name, &exceptions_type.wrapper_name
                    ),
                    rust_type: RustWrapperType::Result(ok_type.boxed(), exceptions_type.boxed()),
                    is_ref: false,
                    is_mut: false,
                })
            },
        )
    }

    fn parse_option_wrapper(
        &mut self,
        original_type: Type,
        path_segment: &PathSegment,
    ) -> anyhow::Result<WrapperType> {
        ExternModuleTranslator::get_inner_generic_type(path_segment)
            .and_then(|inner_path| self.parse_and_register_rust_type(inner_path))
            .map(|inner_type_name| WrapperType {
                original_type_name: original_type,
                wrapper_name: format!("Optional{}", &inner_type_name.wrapper_name),
                rust_type: RustWrapperType::Option(inner_type_name.boxed()),
                is_ref: false,
                is_mut: false,
            })
    }

    fn parse_vec_wrapper(
        &mut self,
        original_type: Type,
        path_segment: &PathSegment,
    ) -> anyhow::Result<WrapperType> {
        ExternModuleTranslator::get_inner_generic_type(path_segment)
            .and_then(|inner_path| self.parse_and_register_rust_type(inner_path))
            .map(|inner_type_name| WrapperType {
                original_type_name: original_type,
                wrapper_name: format!("Vec{}", &inner_type_name.wrapper_name),
                rust_type: RustWrapperType::Vector(inner_type_name.boxed()),
                is_ref: false,
                is_mut: false,
            })
    }

    pub fn get_enum(&self, ident_str: &str) -> Option<&ItemEnum> {
        self.shared_enums
            .iter()
            .find(|enum_item| enum_item.ident == ident_str)
    }
}

fn parse_arc_wrapper(
    original_type: Type,
    path_segment: &PathSegment,
) -> anyhow::Result<WrapperType> {
    ExternModuleTranslator::get_inner_generic_type(path_segment).map(|inner_path| {
        let generated_inner_type_name = inner_path
            .to_token_stream()
            .to_string()
            .replace("dyn", "")
            .replace(['<', '>', ' '], "");
        WrapperType {
            original_type_name: original_type,
            wrapper_name: format!("Shared{}", &generated_inner_type_name),
            rust_type: RustWrapperType::Shared,
            is_ref: false,
            is_mut: false,
        }
    })
}

fn parse_string_wrapper(original_type: Type) -> WrapperType {
    WrapperType {
        original_type_name: original_type,
        wrapper_name: "RustString".to_string(),
        rust_type: RustWrapperType::String,
        is_ref: false,
        is_mut: false,
    }
}

fn parse_primitive_wrapper(original_type: Type, primitive: &str) -> WrapperType {
    WrapperType {
        original_type_name: original_type,
        wrapper_name: primitive.to_owned(),
        rust_type: RustWrapperType::Primitive,
        is_ref: false,
        is_mut: false,
    }
}

fn parse_custom_wrapper(original_type: Type, path_segment: &PathSegment) -> WrapperType {
    WrapperType {
        original_type_name: original_type,
        wrapper_name: path_segment.ident.to_string(),
        rust_type: RustWrapperType::Custom,
        is_ref: false,
        is_mut: false,
    }
}

fn parse_enum_wrapper(enum_item: &ItemEnum) -> WrapperType {
    let ident = &enum_item.ident;
    WrapperType {
        original_type_name: parse_quote! {#ident},
        wrapper_name: ident.to_string(),
        rust_type: if is_primitive_enum(enum_item) {
            RustWrapperType::FieldlessEnum
        } else {
            RustWrapperType::DataEnum
        },
        is_ref: false,
        is_mut: false,
    }
}

pub fn exception_wrapper_name(enum_ident: &str) -> String {
    format!("{}Exceptions", &enum_ident)
}
