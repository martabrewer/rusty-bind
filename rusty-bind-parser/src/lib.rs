//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod binding_module;
pub mod binding_types;
pub mod cpp;
pub mod enum_helpers;
pub mod extern_functions_utils;
pub mod extern_module_translator;
mod ordered_hash_set;
pub mod swift;
mod utils;

pub use binding_module::BindingModule;
pub use binding_module::BuildContext;

const EXPORTED_SYMBOLS_PREFIX: &str = "__rustybind__";
