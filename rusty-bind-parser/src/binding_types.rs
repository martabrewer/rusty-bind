//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use proc_macro2::{Ident, TokenStream};
use std::hash::Hash;
use syn::Type;

#[derive(Hash, PartialEq, Eq, Clone, Debug)]
pub enum Exceptions {
    Primitive(Vec<Ident>),
    NonPrimitive(Vec<Ident>),
}

#[derive(Hash, PartialEq, Eq, Clone, Debug)]
pub enum RustWrapperType {
    Result(Box<WrapperType>, Box<WrapperType>),
    Option(Box<WrapperType>),
    Vector(Box<WrapperType>),
    Shared,
    Custom,
    String,
    Primitive,
    Trait,
    FieldlessEnum,
    DataEnum,
    Exceptions(Exceptions),
    ExceptionTrait,
}

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct WrapperType {
    pub original_type_name: Type,
    pub wrapper_name: String,
    pub rust_type: RustWrapperType,
    pub is_ref: bool,
    pub is_mut: bool,
}

impl WrapperType {
    pub fn boxed(self) -> Box<WrapperType> {
        Box::new(self)
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Arg {
    pub arg_name: String,
    pub typ: WrapperType,
}

#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct Function {
    pub arguments: Vec<Arg>,
    pub return_type: Option<WrapperType>,
    pub name: String,
}

#[derive(Clone)]
pub struct ExternFunction {
    pub arguments: Vec<WrapperType>,
    pub return_type: Option<WrapperType>,
    pub name: String,
    pub tokens: TokenStream,
}
