use crate::{
    enum_helpers::{enum_tag_name, is_primitive_enum},
    extern_module_translator::ExternModuleTranslator,
};
use std::fmt::Display;
use syn::ItemEnum;

/// Translates rust enum into its C equivalents.
/// For any Rust enum only a C enum class representation is generated
/// (for both unit and field enums).
///
pub fn translate_c_enums(emt: &ExternModuleTranslator) -> String {
    emt.shared_enums
        .iter()
        .map(|e| {
            if is_primitive_enum(e) {
                translate_c_enum(e)
            } else {
                create_complex_enum_tag(e)
            }
        })
        .collect()
}

fn translate_c_enum(enum_item: &ItemEnum) -> String {
    let enum_name = enum_item.ident.to_string();
    let variants = enum_item.variants.iter().map(|variant| &variant.ident);
    create_c_enum_class(&enum_name, variants)
}

fn create_complex_enum_tag(enum_item: &ItemEnum) -> String {
    let enum_tag_name = enum_tag_name(enum_item.ident.to_string().as_str());
    let variants = enum_item.variants.iter().map(|variant| &variant.ident);
    create_c_enum_class(&enum_tag_name, variants)
}

pub fn create_c_enum_class<T: Display>(name: &str, variants: impl Iterator<Item = T>) -> String {
    let variants = variants
        .map(|id| format!("    {name}_{id}"))
        .collect::<Vec<_>>()
        .join(",\n");
    format!("\nenum {name} {{\n{variants}\n}};\n")
}

#[cfg(test)]
mod tests {
    use crate::utils::helpers;

    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_creating_enum_class_definition() {
        let enum_item = helpers::get_enum_item();
        let definition = translate_c_enum(&enum_item);

        let expected_definition = "
enum En1 {
    En1_V1,
    En1_V2
};
";
        assert_eq!(definition, expected_definition);
    }
}
