use crate::extern_module_translator::{RustWrapperType, WrapperType};
use crate::EXPORTED_SYMBOLS_PREFIX;

pub trait TargetLanguageTypeName {
    fn get_name(&self) -> String;
    fn get_name_for_abstract_method(&self) -> String;
}

impl TargetLanguageTypeName for WrapperType {
    fn get_name(&self) -> String {
        match self {
            WrapperType {
                rust_type: RustWrapperType::Vector(inner_type),
                ..
            } => {
                let inner_name = inner_type.get_name();
                format!("RustVec<{inner_name}>")
            }
            WrapperType {
                rust_type: RustWrapperType::Option(inner_type),
                ..
            } => {
                let inner_name = inner_type.get_name();
                format!("RustOptional<{inner_name}>")
            }
            WrapperType {
                rust_type: RustWrapperType::Result(ok_type, _),
                ..
            } => ok_type.get_name(),
            _ => self.wrapper_name.clone(),
        }
    }
    fn get_name_for_abstract_method(&self) -> String {
        match self {
            WrapperType {
                rust_type: RustWrapperType::Vector(inner_type),
                ..
            } => {
                let inner_name = inner_type.get_name();
                format!("RustVec<{inner_name}>")
            }
            WrapperType {
                rust_type: RustWrapperType::Option(inner_type),
                ..
            } => {
                let inner_name = inner_type.get_name();
                format!("RustOptional<{inner_name}>")
            }
            _ => self.wrapper_name.clone(),
        }
    }
}

/// Predefined C++ classes and primitive types typedefs useful
/// during the standard work on FFI basic structures. It
/// consists of a custom RustVector and String implementations
/// that keep track of some object ownership aspects. Items
/// returned by `RustVec::at(x)` method should be treated as
/// references, since the underlying objects are owned by
/// the vector.
///
pub const RUST_EXCEPTION_BASE_CLASS_NAME: &str = "RustExceptionBase";
pub const PREDEFINED: &str = const_format::formatcp!(
    r#"
import Foundation

public typealias bool = Bool

open class Opaque {{
    fileprivate var _self: UnsafeMutableRawPointer
    fileprivate let is_owned: Bool

    public required init(_ _self: UnsafeMutableRawPointer, _ is_owned: Bool = true) {{
        self._self = _self
        self.is_owned = is_owned
    }}

    open func asRef() -> UnsafeMutableRawPointer {{
        return self._self
    }}
}}

protocol ToOwned {{
    func toOwned() -> UnsafeMutableRawPointer;
}}

func convertToOwnedType<T>(_ input: T) -> T {{
    input
}}

func convertToOwnedType<T: ToOwned>(_ input: T) -> UnsafeMutableRawPointer {{
    input.toOwned()
}}

public protocol OptionalProtocol {{
    static func createOptional(_ ptr: Self) -> UnsafeMutableRawPointer;
    static func createEmptyOptional() -> UnsafeMutableRawPointer;
    static func decomposeOptional(_ ptr: UnsafeMutableRawPointer);
    static func optionCopy(_ ptr: UnsafeMutableRawPointer) -> UnsafeMutableRawPointer;
    static func optionUnwrap(_ ptr: UnsafeMutableRawPointer) -> Self;
    static func optionIsSome(_ ptr: UnsafeMutableRawPointer) -> Bool;
}}

extension OptionalProtocol {{
    public static func createOptional(_ ptr: Self) -> UnsafeMutableRawPointer {{
        fatalError("OptionalProtocol not implemented for the given type")
    }}
    public static func createEmptyOptional() -> UnsafeMutableRawPointer {{
        fatalError("OptionalProtocol not implemented for the given type")
    }}
    public static func decomposeOptional(_ ptr: UnsafeMutableRawPointer) {{
        fatalError("OptionalProtocol not implemented for the given type")
    }}
    public static func optionCopy(_ ptr: UnsafeMutableRawPointer) -> UnsafeMutableRawPointer {{
        fatalError("OptionalProtocol not implemented for the given type")
    }}
    public static func optionUnwrap(_ ptr: UnsafeMutableRawPointer) -> Self {{
        fatalError("OptionalProtocol not implemented for the given type")
    }}
    public static func optionIsSome(_ ptr: UnsafeMutableRawPointer) -> Bool {{
        fatalError("OptionalProtocol not implemented for the given type")
    }}
}}

public class RustOptional<T: OptionalProtocol>: Opaque, ToOwned {{
    deinit {{
        if(is_owned) {{
            T.decomposeOptional(self._self)
        }}
    }}

    public func swiftOptional() -> Optional<T> {{
        if (T.optionIsSome(self._self)) {{
            let result = Optional.some(T.optionUnwrap(self.toOwned()))
            return result
        }} else {{
            return nil
        }}
    }}

    func toOwned() -> UnsafeMutableRawPointer {{
        T.optionCopy(self._self)
    }}
}}

public protocol RustVectorProtocol: OptionalProtocol {{
    static func createNewRustVec() -> UnsafeMutableRawPointer;
    static func decomposeRustVec(_ ptr: UnsafeMutableRawPointer);
    static func vecCopy(_ ptr: UnsafeMutableRawPointer) -> UnsafeMutableRawPointer;
    static func vecPush(_ ptr: UnsafeMutableRawPointer, _ item: Self);
    static func vecAt(_ ptr: UnsafeMutableRawPointer, _ index: usize) -> UnsafeMutableRawPointer;
    static func vecSize(_ ptr: UnsafeMutableRawPointer) -> usize;
}}

extension RustVectorProtocol {{
    public static func createNewRustVec() -> UnsafeMutableRawPointer {{
        fatalError("RustVectorProtocol not implemented for the given type")
    }}
    public static func decomposeRustVec(_ ptr: UnsafeMutableRawPointer) {{
        fatalError("RustVectorProtocol not implemented for the given type")
    }}
    public static func vecCopy(_ ptr: UnsafeMutableRawPointer) -> UnsafeMutableRawPointer {{
        fatalError("RustVectorProtocol not implemented for the given type")
    }}
    public static func vecPush(_ ptr: UnsafeMutableRawPointer, _ item: Self) {{
        fatalError("RustVectorProtocol not implemented for the given type")
    }}
    public static func vecAt(_ ptr: UnsafeMutableRawPointer, _ index: usize) -> UnsafeMutableRawPointer {{
        fatalError("RustVectorProtocol not implemented for the given type")
    }}
    public static func vecSize(_ ptr: UnsafeMutableRawPointer) -> usize {{
        fatalError("RustVectorProtocol not implemented for the given type")
    }}
}}

open class RustVec<T: RustVectorProtocol>: Opaque, ToOwned, Collection {{
    public typealias Index = usize
    public typealias Element = T
    public var startIndex: Index {{ return 0 }}
    public var endIndex: Index {{ return self.size() }}

    open subscript(index: Index) -> T {{
        return self.at(index)!
    }}

    open func index(after i: Index) -> Index {{
        return i + 1
    }}

    deinit {{
        if(is_owned) {{
            T.decomposeRustVec(self._self)
        }}
    }}

    public convenience init() {{
        self.init(T.createNewRustVec())
    }}

    public convenience init<I: Sequence>(_ iterable: I) where I.Element == T {{
        let _self = T.createNewRustVec()
        self.init(_self)
        for elem in iterable {{
            self.push(elem)
        }}
    }}

    open func push(_ item: T) {{
        T.vecPush(self._self, item)
    }}

    open func at(_ index: usize) -> Optional<T> {{
        let opt = T.vecAt(self._self, index)
        if (T.optionIsSome(opt)) {{
            let result = Optional.some(T.optionUnwrap(opt))
            return result
        }} else {{
            T.decomposeOptional(opt)
            return nil
        }}
    }}

    open func size() -> usize {{
        T.vecSize(self._self)
    }}

    func toOwned() -> UnsafeMutableRawPointer {{
        T.vecCopy(self._self)
    }}
}}

extension RustVec: RustVectorProtocol {{ }}
extension RustVec: OptionalProtocol {{ }}
extension RustOptional: RustVectorProtocol {{ }}
extension RustOptional: OptionalProtocol {{ }}

extension Optional where Wrapped: OptionalProtocol {{
    public func toRustOptional() -> RustOptional<Wrapped> {{
        if let some_value = self {{
            return RustOptional<Wrapped>(Wrapped.createOptional(convertToOwnedType(some_value)))
        }} else {{
            return RustOptional<Wrapped>(Wrapped.createEmptyOptional())
        }}
    }}
}}

public class RustString: Opaque {{
    public convenience init(_ str: String) {{
        let rust_string_ptr: UnsafeMutableRawPointer = str.utf8CString.withUnsafeBufferPointer({{bufferPtr in
            let rustStringPtr = {EXPORTED_SYMBOLS_PREFIX}$RustString$from_c_str(
                UnsafeMutableRawPointer(mutating: bufferPtr.baseAddress!)
            )
            return rustStringPtr!
        }})
        self.init(rust_string_ptr)
    }}
    public func toString() -> String {{
        let str = {EXPORTED_SYMBOLS_PREFIX}$RustString$as_mut_ptr(self._self).assumingMemoryBound(to: UInt8.self)
        let length = {EXPORTED_SYMBOLS_PREFIX}$RustString$len(self._self)
        let bytes: UnsafeBufferPointer<UInt8> = UnsafeBufferPointer(start: str, count: Int(length))
        return String(bytes: bytes, encoding: .utf8)!
    }}
    deinit {{
        if(is_owned) {{
            {EXPORTED_SYMBOLS_PREFIX}$RustString$drop(self._self);
        }}
    }}
}}

extension RustString: ToOwned {{
    func toOwned() -> UnsafeMutableRawPointer {{
        {EXPORTED_SYMBOLS_PREFIX}$RustString$clone(self._self)
    }}
}}

"#
);

/// Creates an implementation of RustVec<T> for a given T that maps to the Rust Vec<T> object.
///
pub fn vector_impl(inner_type: &str, rust_wrapper_name: &str, is_generic: bool) -> String {
    let definition_start = if is_generic {
        format!("extension RustVectorProtocol where Self == {inner_type} {{")
    } else {
        format!("extension {inner_type}: RustVectorProtocol {{")
    };
    format!(
        "
{definition_start}
    public static func createNewRustVec() -> UnsafeMutableRawPointer {{
        return {EXPORTED_SYMBOLS_PREFIX}$Vec{rust_wrapper_name}$new();
    }}
    public static func decomposeRustVec(_ ptr: UnsafeMutableRawPointer) {{
        {EXPORTED_SYMBOLS_PREFIX}$Vec{rust_wrapper_name}$drop(ptr)
    }}
    public static func vecCopy(_ ptr: UnsafeMutableRawPointer) -> UnsafeMutableRawPointer {{
        return {EXPORTED_SYMBOLS_PREFIX}$Vec{rust_wrapper_name}$clone(ptr);
    }}
    public static func vecPush(_ ptr: UnsafeMutableRawPointer, _ item: {inner_type}) {{
        {EXPORTED_SYMBOLS_PREFIX}$Vec{rust_wrapper_name}$push(ptr, convertToOwnedType(item))
    }}
    public static func vecAt(_ ptr: UnsafeMutableRawPointer, _ index: usize) -> UnsafeMutableRawPointer {{
        return {EXPORTED_SYMBOLS_PREFIX}$Vec{rust_wrapper_name}$get(ptr, index)
    }}
    public static func vecSize(_ ptr: UnsafeMutableRawPointer) -> usize {{
        return {EXPORTED_SYMBOLS_PREFIX}$Vec{rust_wrapper_name}$len(ptr)
    }}
}}
"
    )
}

/// Creates a class that maps onto the Rust Option<T> object.
/// The ownership depends on the source from which the object is created.
///
pub fn option_class(inner_type: &str, rust_wrapper_name: &str, is_generic: bool) -> String {
    let definition_start = if is_generic {
        format!("extension OptionalProtocol where Self == {inner_type} {{")
    } else {
        format!("extension {inner_type}: OptionalProtocol {{")
    };
    format!(
        "
{definition_start}
    public static func createOptional(_ ptr: {inner_type}) -> UnsafeMutableRawPointer {{
        return {EXPORTED_SYMBOLS_PREFIX}$Optional{rust_wrapper_name}$from(convertToOwnedType(ptr))
    }}
    public static func createEmptyOptional() -> UnsafeMutableRawPointer {{
        return {EXPORTED_SYMBOLS_PREFIX}$Optional{rust_wrapper_name}$default()
    }}
    public static func decomposeOptional(_ ptr: UnsafeMutableRawPointer) {{
        {EXPORTED_SYMBOLS_PREFIX}$Optional{rust_wrapper_name}$drop(ptr)
    }}
    public static func optionCopy(_ ptr: UnsafeMutableRawPointer) -> UnsafeMutableRawPointer {{
        return {EXPORTED_SYMBOLS_PREFIX}$Optional{rust_wrapper_name}$clone(ptr);
    }}
    public static func optionUnwrap(_ ptr: UnsafeMutableRawPointer) -> Self {{
        let cloned = {EXPORTED_SYMBOLS_PREFIX}$Optional{rust_wrapper_name}$clone(ptr)
        return Self({EXPORTED_SYMBOLS_PREFIX}$Optional{rust_wrapper_name}$unwrap(cloned))
    }}
    public static func optionIsSome(_ ptr: UnsafeMutableRawPointer) -> Bool {{
        return {EXPORTED_SYMBOLS_PREFIX}$Optional{rust_wrapper_name}$is_some(ptr)
    }}
}}
"
    )
}

/// Creates a class that maps onto the Rust Result<T, ErrorType> object.
/// The ownership depends on the source from which the object is created.
///
pub fn result_class(name: &str, ok_type: &str, err_type: &str) -> String {
    format!(
        "
public class {name}: Opaque {{
    deinit {{
        if(is_owned) {{
            {EXPORTED_SYMBOLS_PREFIX}${name}$drop(self._self)
        }}
    }}
    public class func from_ok(_ val: {ok_type}) -> {name} {{
        return {name}({EXPORTED_SYMBOLS_PREFIX}${name}$from_ok(convertToOwnedType(val)))
    }}
    public class func from_err(_ val: {err_type}) -> {name} {{
        return {name}({EXPORTED_SYMBOLS_PREFIX}${name}$from_err(convertToOwnedType(val)))
    }}
    public func unwrap() -> {ok_type} {{
        let cloned = {EXPORTED_SYMBOLS_PREFIX}${name}$clone(self._self)
        return {ok_type}({EXPORTED_SYMBOLS_PREFIX}${name}$unwrap(cloned))
    }}
    public func unwrapErr() -> {err_type} {{
        let cloned = {EXPORTED_SYMBOLS_PREFIX}${name}$clone(self._self)
        return {err_type}({EXPORTED_SYMBOLS_PREFIX}${name}$unwrap_err_unchecked(cloned))
    }}
    public func isOk() -> Bool {{
        return {EXPORTED_SYMBOLS_PREFIX}${name}$is_ok(self._self)
    }}
}}

extension {name}: ToOwned {{
    func toOwned() -> UnsafeMutableRawPointer {{
        {EXPORTED_SYMBOLS_PREFIX}${name}$clone(self._self)
    }}
}}
"
    )
}

/// Creates a class that maps onto the custom structures with methods
/// that are available in the FFI.
/// The ownership depends on the source from which the object is created.
///
pub fn custom_class_definition(name: &str, functions_definitions: &str) -> String {
    format!(
        "
public class {name}: Opaque {{
    deinit {{
        if(is_owned) {{
            {EXPORTED_SYMBOLS_PREFIX}${name}$drop(self._self)
        }}
    }}
{functions_definitions}}};

extension {name}: ToOwned {{
    func toOwned() -> UnsafeMutableRawPointer {{
        {EXPORTED_SYMBOLS_PREFIX}${name}$clone(self._self)
    }}
}}

"
    )
}

/// Creates a class that maps onto the custom structures with methods
/// that are available in the FFI.
/// The ownership depends on the source from which the object is created.
///
pub fn abstract_class_declaration(name: &str, functions_declaration: &str) -> String {
    format!(
        "
open class {name} {{
    public init() {{}}
{functions_declaration}}}

extension {name}: ToOwned {{
    func toOwned() -> UnsafeMutableRawPointer {{
        UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
    }}
}}
\n"
    )
}
