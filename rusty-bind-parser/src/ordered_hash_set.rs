//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{collections::HashSet, hash::Hash};

#[derive(Clone)]
pub struct OrderedHashSet<T> {
    set: HashSet<T>,
    ordered: Vec<T>,
}

impl<T> Default for OrderedHashSet<T> {
    fn default() -> Self {
        Self {
            set: Default::default(),
            ordered: Default::default(),
        }
    }
}

impl<T> OrderedHashSet<T>
where
    T: Eq + Hash + Clone,
{
    pub fn insert(&mut self, item: T) {
        if self.set.insert(item.clone()) {
            self.ordered.push(item)
        }
    }

    pub fn unordered_iter(&self) -> impl Iterator<Item = &T> {
        self.set.iter()
    }

    pub fn ordered_iter(&self) -> impl Iterator<Item = &T> {
        self.ordered.iter()
    }
}
