//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::{
    binding_types::{Arg, Function, RustWrapperType, WrapperType},
    cpp::templates::TargetLanguageTypeName,
    enum_helpers::enum_tag_name,
    extern_module_translator::Exceptions,
    EXPORTED_SYMBOLS_PREFIX,
};
use std::fmt::Display;

/// The structure contains already translated elements of a function.
/// This can be usuful to generate declaration and definition of the
/// given function in two different places.
///
#[derive(Clone)]
pub struct FunctionTranslator {
    pub function_name: String,
    pub generated_args: String,
    pub generated_function_body: Vec<String>,
    pub return_type: Option<WrapperType>,
    class_name: Option<String>,
}

impl FunctionTranslator {
    /// Translates intermediate form of a class method into the C++ glue code.
    ///
    pub fn from_class_method(function: &Function, class_name: &str) -> Self {
        FunctionTranslator::from_function(function, Some(class_name.to_owned()))
    }

    /// Translates intermediate form of a global function into the C++ glue code.
    ///
    pub fn from_global_function(function: &Function) -> Self {
        FunctionTranslator::from_function(function, None)
    }

    /// Translates the intermediate form of a parsed function into
    /// the elements ready-to-use in the C++ code generation process.
    ///
    fn from_function(function: &Function, class_name: Option<String>) -> Self {
        let generated_args: String = function
            .arguments
            .iter()
            .skip(class_name.is_some() as usize)
            .map(|arg| {
                let optional_ref_token = if arg.typ.is_ref { "&" } else { "" };
                let arg_name = &arg.arg_name;
                let return_type_string = arg.typ.get_name();
                format!("{return_type_string}{optional_ref_token} {arg_name}")
            })
            .collect::<Vec<String>>()
            .join(", ");
        let generated_function_body: Vec<String> = function
            .arguments
            .iter()
            .skip(class_name.is_some() as usize)
            .map(map_function_argument)
            .collect();
        let function_name = function.name.to_string();
        FunctionTranslator {
            function_name,
            generated_args,
            generated_function_body,
            return_type: function.return_type.clone(),
            class_name,
        }
    }

    /// Generates a function declaration that can be used
    /// within a class declaration or for global functions.
    ///
    pub fn generate_declaration(self) -> String {
        let FunctionTranslator {
            function_name,
            generated_args,
            generated_function_body: _,
            return_type,
            ..
        } = self;
        let return_type_string = return_type
            .as_ref()
            .map(|w| w.get_name())
            .unwrap_or_else(|| "void".to_string());
        format!("    {return_type_string} {function_name}({generated_args});\n")
    }

    /// Generates a method or global function definition.
    ///
    pub fn generate_definition(self) -> String {
        let FunctionTranslator {
            function_name,
            generated_args,
            return_type,
            ..
        } = &self;

        let class_name_path = self.class_name_path();
        let class_function_name = self.class_function_name();
        let ffi_call = self.ffi_call();

        if let Some(WrapperType {
            wrapper_name,
            rust_type: RustWrapperType::Result(ok_type, err_type),
            ..
        }) = return_type
        {
            self.generate_throwing_definition(
                wrapper_name,
                ok_type,
                err_type,
                &err_type.wrapper_name,
            )
        } else {
            let return_type_string = return_type
                .as_ref()
                .map(|w| w.get_name())
                .unwrap_or_else(|| "void".to_string());
            format!("{return_type_string} {class_name_path}{function_name}({generated_args}) {{
    return {return_type_string}({EXPORTED_SYMBOLS_PREFIX}${class_function_name}{function_name}({ffi_call}));\n}}\n")
        }
    }

    fn generate_throwing_definition(
        &self,
        result_wrapper_name: &str,
        ok_type: &WrapperType,
        exceptions: &WrapperType,
        exceptions_enum_name: &str,
    ) -> String {
        let FunctionTranslator {
            function_name,
            generated_args,
            ..
        } = self;
        let class_name_path = self.class_name_path();
        let class_function_name = self.class_function_name();
        let ffi_call = self.ffi_call();
        let return_type_string = ok_type.get_name();

        let exceptions_throws = match &exceptions.rust_type {
            RustWrapperType::Exceptions(Exceptions::Primitive(idents)) => idents
                .iter()
                .map(|exception| {
                    let variant_check = format!(" == {exceptions_enum_name}::{exception}");
                    exception_throw(&exceptions.wrapper_name, exception, variant_check)
                })
                .collect::<String>(),
            RustWrapperType::Exceptions(Exceptions::NonPrimitive(idents)) => idents
                .iter()
                .map(|exception| {
                    let enum_tag_name = enum_tag_name(exceptions_enum_name);
                    let variant_check = format!(".get_tag() == {enum_tag_name}::{exception}");
                    exception_throw(&exceptions.wrapper_name, exception, variant_check)
                })
                .collect::<String>(),

            _ => panic!("Invalid wrapper type for exceptions wrapper"),
        };

        format!(
            "
{return_type_string} {class_name_path}{function_name}({generated_args}) {{
    auto result = {result_wrapper_name}(__rustybind__${class_function_name}{function_name}({ffi_call}));
    if (result.is_ok()) {{
        return result.unwrap();
    }} {exceptions_throws}
    else {{
        throw std::logic_error(\"Unknown to ffi layer exception has been thrown\");
    }}
}}
"
        )
    }

    fn class_name_path(&self) -> String {
        self.class_name
            .as_ref()
            .map_or("".to_string(), |class_name| format!("{class_name}::"))
    }

    fn class_function_name(&self) -> String {
        self.class_name
            .as_ref()
            .map_or("".to_string(), |class_name| format!("{class_name}$"))
    }

    fn ffi_call(&self) -> String {
        let mut ffi_call_items = self.generated_function_body.clone();
        if self.class_name.is_some() {
            ffi_call_items.insert(0, "this->self".to_owned());
        }
        ffi_call_items.join(", ")
    }
}

fn exception_throw(
    err_enum_name: impl Display,
    exception: impl Display,
    variant_check: impl Display,
) -> String {
    format!(
        "
    else if (result.unwrap_err(){variant_check}) {{
        throw {err_enum_name}_{exception}Exception(result.unwrap_err());
    }}"
    )
}

fn map_function_argument(arg: &Arg) -> String {
    if arg.typ.is_ref
        && arg.typ.rust_type != RustWrapperType::Primitive
        && arg.typ.rust_type != RustWrapperType::FieldlessEnum
        && arg.typ.rust_type != RustWrapperType::Trait
    {
        format!("{}.as_ref()", arg.arg_name)
    } else {
        arg.arg_name.to_owned()
    }
}
