//
// Wildland Project
//
// Copyright © 2022 Golem Foundation,
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as published by
// the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod enums;
pub mod externs;
mod function_translator;
mod function_virtual_translator;
pub mod generator;
pub mod swig_generator;
pub mod templates;
pub mod wasm_generator;

pub use enums::*;
pub use function_virtual_translator::FunctionVirtualTranslator;
pub use generator::generate_cpp_file;
pub use templates::exception_class_name;
