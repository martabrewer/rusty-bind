# Contribution policy
RustyBind has been started and developed in Wildland Foundation. As a Contributor you agree on the [Wildland Contributor Agreement](https://docs.wildland.io/contributor-agreement.html).


## Testing
Probably the shortest way to run the tests is to use a docker image as a builder environment (`Dockerfile.builder`) and to run the `example/test_script.sh` within the builder container. It contains all the required tools that are needed to build FFI glue-code for every supported language and run tests/examples.

There are also a few unit tests that can be run by using `cargo test` command. It is worth to note that currently the main accent is put on the integration tests placed in the `example/test_interface` demo-project rather than Rust unit-tests. Although it is planned to increase the code coverage by adding more module-level tests in the Rust source files.


## Bug reporting
It is more than welcome to report any issue encountered when using RustyBind. Best way to do it is to create a gitlab issue consisting of every detail, code or test snippet that could be useful to reproduce the bug and add a proper scenario to the regression testsuite.
